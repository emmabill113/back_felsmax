import React from 'react';
import { Box, Grid } from "@mui/material";
import { BarChart, Bar, CartesianGrid, XAxis, YAxis, Tooltip, Legend, ResponsiveContainer } from 'recharts';
import "./BarCharts.css"

const BarCharts = ({ transactions }) => {
  
 const groupTransactions = () => {
  const groupedData = {};

  const currentYear = new Date().getFullYear(); // Obtient l'année en cours

  transactions.forEach(transaction => {
    const date = new Date(transaction.createdAt);
    const year = date.getFullYear(); // Obtient l'année de la transaction
    const month = date.toLocaleString('default', { month: 'long' });
    const operator = transaction.operator;
    const amount = transaction.amount;

    // Vérifie si la transaction appartient à l'année en cours
    if (year === currentYear) {
      if (!groupedData[month]) {
        groupedData[month] = {};
      }

      if (!groupedData[month][operator]) {
        groupedData[month][operator] = 0;
      }

      groupedData[month][operator] += 1; // Comptage du nombre de transactions
    }
  });

  const sortedMonths = Object.keys(groupedData).sort((a, b) => {
    const monthA = new Date(`${a} 1, ${currentYear}`);
    const monthB = new Date(`${b} 1, ${currentYear}`);
    return monthA - monthB;
  });
  

  const chartData = sortedMonths.map(month => ({
    name: month,
    FELSMAX: groupedData[month].FELSMAX,
    ORANGE: groupedData[month].ORANGE_MONEY,
    EU: groupedData[month].EXPRESS_UNION,
    UBA: groupedData[month].UBA,
    UBA: groupedData[month].MTN_MONEY,
    YOOMEE : groupedData[month].YOOMEE,

    ...groupedData[month],
  }));

  return chartData;
};

  const chartData = groupTransactions();
  console.log('ici les data fdfdfdf',chartData);

  return (
    <>
      <div className='chartTitle'>
        <p style={{ fontWeight: "bolder", fontSize: "18px", marginTop: 0, marginBottom: 0 }}>
          <strong>Nombres de transactions</strong> (XAF)
        </p>
        <p style={{ marginTop: 2, fontSize: "14px" }}> classé par mois et par operateur (ORANGE, MTN, EU, UBA,YOOMEE)</p>
      </div>
      <ResponsiveContainer width="100%" height="80%">
        <BarChart
          width={300}
          height={200}
          data={chartData}
          margin={{
            top: 5,
            right: 30,
            left: 20,
            bottom: 5,
          }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" />
          <YAxis />
          <Tooltip contentStyle={{borderRadius:15}} />
          <Legend />
          <Bar dataKey="FELSMAX" fill="black" barSize={15} shape="round" barGap={5} />
          <Bar dataKey="EU" fill="blue" barSize={15} shape="round" barGap={1} />
          <Bar dataKey="ORANGE" fill="#fb8500" barSize={15} shape="round" barGap={1} />
          <Bar dataKey="UBA" fill="#f74949" barSize={15} shape="round" barGap={1} />
          <Bar dataKey="MTN" fill="#ffc300" barSize={15} shape="round" barGap={1} />
          <Bar dataKey="YOOMEE" fill="#d90429" barSize={15} shape="round" barGap={1} />
        </BarChart>
      </ResponsiveContainer>
    </>
  );
}

export default BarCharts;
