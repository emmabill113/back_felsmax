import React, { useState } from 'react';
import { Grid } from '@mui/material';
import ListItemProjetGroup from '../ListItem';
import TransactionModalProjetGroup from '../ListItem/GroupProjectTransactions';
import { connect } from "react-redux";
import './index.css'
 

const Tontine = ({userGroupes, laoding}) => {
  console.log('ici la liste des groupe', userGroupes)
  
  const [selectItemId, setselectItemId] = useState(null);

  const handleView = (userId) => {
    setselectItemId(userId);
  };

  const handleCloseModal = () => {
    setselectItemId(null);
  };

  const option= [
    {
      value: 'tous', label: 'Tous'
    }, 
    {
      value: 'Gage', label: 'Gage'
    }, 
    {
      value: 'Tontine', label: 'Tontine'
    },
  ]


  const getUserById = (userId) => {
    return userGroupes.find(user => user._id === userId);
  };

  const selectItem = selectItemId ? getUserById(selectItemId) : null;

  return (
    <Grid style={{padding:'10px'}} container spacing={2} justifyContent="center">
      <ListItemProjetGroup  options={option} vue={'groupes'} itemList={userGroupes} handleView={handleView}/>
      <TransactionModalProjetGroup vue={'groupe'} operateur={selectItem?.transactions} handleCloseModal={handleCloseModal} selectItem={selectItem} selectItemId={selectItemId}/>
    </Grid>
  );
};


const mapStateToProps = ({ AuthReducer }) => ({
  userGroupes: AuthReducer.userGroupes,
  laoding: AuthReducer.laoding
})
export default connect(mapStateToProps)(Tontine);
