import React, { useState } from "react";
import { Modal, Box, Typography, Button, Grid } from "@mui/material";
import renderTextField from "../TextFieldIput";
import LoadingButton from "@mui/lab/LoadingButton";
import { connect } from "react-redux";


const EditModal = ({
  open,
  onClose,
  handleEdit,
  title,
  loading,
  fields,
  editedFields,
  setEditedFields,
  sousTilte,
}) => {

  console.log('loading', loading)

  const handleFieldChange = (fieldKey) => (e) => {
    setEditedFields((prevFields) => ({
      ...prevFields,
      [fieldKey]: e.target.value,
    }));
  };

  const handleEditClick = () => {
    const formData = new FormData();
    for (const key in editedFields) {
      formData.append(key, editedFields[key]);
    }
    handleEdit(formData);
  };

  const isFieldsEmpty = Object.values(editedFields).every((value) =>
    Boolean(value)
  );

  return (
    <Modal
      open={open}
      onClose={onClose}
      aria-labelledby="edit-modal-title"
      aria-describedby="edit-modal-description"
    >
      <Box
        sx={{
          position: "absolute",
          top: "50%",
          left: "50%",
          transform: "translate(-50%, -50%)",
          bgcolor: "background.paper",
          boxShadow: 24,
          p: 4,
          maxWidth: "100%",
          borderRadius: 4,
        }}
      >
        <Typography
          className="titre-user"
          variant="h6"
          id="edit-modal-title"
          gutterBottom
        >
          {title}{" "}
          <span style={{ marginLeft: 8, color: "#f74949" }}>{sousTilte}</span>
        </Typography>
        <Grid container style={{ justifyContent: "space-around" }} spacing={2}>
          {fields?.map((field, index) => (
            <React.Fragment key={index}>
              {renderTextField(
                field.label,
                editedFields[field.stateKey] || "",
                handleFieldChange(field.stateKey)
              )}
            </React.Fragment>
          ))}
        </Grid>
        <Box sx={{ display: "flex", justifyContent: "space-evenly", mt: 2 }}>
          <Button
            className="titre-btn"
            onClick={onClose}
            sx={{ marginRight: 2 }}
            variant="contained"
            color="inherit"
          >
            Annuler
          </Button>
          <LoadingButton
            className="titre-btn"
            onClick={handleEditClick}
            variant="contained"
            loading={loading}
            color={isFieldsEmpty ? "error" : "inherit"}
            disabled={!isFieldsEmpty}
          >
            {loading? "En cours..." : "Valider"}
          </LoadingButton>
        </Box>
      </Box>
    </Modal>
  );
};

const mapStateToProps = ({ AuthReducer }) => ({
  transactions: AuthReducer.transactions,
  userInfo: AuthReducer.userInfo,
  userProfile: AuthReducer.userProfile,
  userProjects: AuthReducer.userProjects,
  userGroupes: AuthReducer.userGroupes,
  loading: AuthReducer.loading,
});

export default connect(mapStateToProps)(EditModal);
