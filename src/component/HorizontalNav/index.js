import React, { useState } from "react";
import { Grid, Avatar, Divider, Badge, IconButton } from "@mui/material";
import { NavLink, useLocation } from "react-router-dom";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import { customStyles } from "../../constant/customStyles";
import NotificationsIcon from "@mui/icons-material/Notifications";
import PersonAdd from "@mui/icons-material/PersonAdd";
import Person from "@mui/icons-material/Person";
import "./HorizontalNav.css";
import PersonIcon from "@mui/icons-material/Person";
import EditModal from "../Edit";
import { connect, useDispatch } from "react-redux";
import { register } from "../../redux/Auth/actions";
import MaterialTooltip from "../Tooltip";
import LogoutRoundedIcon from "@mui/icons-material/LogoutRounded";
import { logout } from "../../redux/Auth/actions";
import { useNavigate } from "react-router-dom";
import RefreshSharpIcon from "@mui/icons-material/RefreshSharp";
import { getUserProfile } from "../../redux/Auth/actions";
import LoadingButton from "@mui/lab/LoadingButton";


const HorizontalNav = ({ userProjects, userProfile, userInfo, loading, userGroupes}) => {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const [editModalOpen, setEditModalOpen] = useState(false);
  const [editedFields, setEditedFields] = useState({
    name: "",
    surname: "",
    city: "",
    district: "",
    email: "",
    password: "",
    phone: "",
    birthday: "",
  });
 // const navigate = useNavigate();
 // const location = useLocation();
  const dispatcher = useDispatch();


  const dispatch = useDispatch();

  const RefreshPage = () =>{

    dispatch(getUserProfile());
    console.log("projets: ", userProjects);
    console.log("info: ", userInfo);
    console.log("profile: ", userProfile);
    console.log("groupes: ", userGroupes);
  }

  const handleLogout = () => {
    console.log("logout");
    dispatcher(logout());
    setAnchorEl(null);
    window.location.href = "/";
  };

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const fields = [
    {
      label: "Nom",
      stateKey: "name",
    },
    {
      label: "Prenom",
      stateKey: "surname",
    },
    {
      label: "email",
      stateKey: "email",
    },
    {
      label: "Téléphone",
      stateKey: "phone",
    },
    {
      label: "Date de naissance",
      stateKey: "birthday",
    },
    {
      label: "Ville",
      stateKey: "city",
    },
    {
      label: "Quatier",
      stateKey: "district",
    },
    {
      label: "Mot de passe",
      stateKey: "password",
    },
    {
      label: "Confirme le mot de passe",
      stateKey: "editedConfirmPass",
    },
    // Ajoutez les autres champs ici
  ];

  const handleEditModalOpen = () => {
    setEditModalOpen(true);
  };

  const handleEditModalClose = () => {
    setEditModalOpen(false);
  };

  const handleEditUser = () => {
    dispatcher(register(editedFields));
    console.log("vous avez cliquer pour enregister un user !");
    handleEditModalClose();
  };

  return (
    <Grid className="horizontalNav">
      <Grid className="welcomeTitle">
        <h3 className="titre-head">BIENVENUE SUR LE BACK OFFICE FELSMAX</h3>
      </Grid>

      <Grid className="horizontalNavContainer">
        <Badge   color="secondary" sx={{ marginRight: "10%" }}>
          <MaterialTooltip title="Actualiser la page">
            <LoadingButton loading={loading} onClick={RefreshPage}>
              {loading?"":<RefreshSharpIcon color="info" sx={customStyles.customIcons}/>}
            </LoadingButton>
          </MaterialTooltip>
        </Badge>
        <Badge
          badgeContent={4}
          overlap="circular"
          color="secondary"
          sx={{ marginRight: "3%" }}
        >
          <li className="horizontalNavItem">
            <NotificationsIcon fontSize="900" sx={customStyles.customIcons} />
          </li>
        </Badge>
        <li className="avatar">
          <MaterialTooltip title="Paramètres">
            <IconButton
              onClick={handleClick}
              size="small"
              sx={{ ml: 2 }}
              aria-controls={open ? "account-menu" : undefined}
              aria-haspopup="true"
              aria-expanded={open ? "true" : undefined}
            >
              <Avatar sx={{ width: 40, height: 40 }}>
                <PersonIcon sx={{ height: 20, width: 20 }} />
              </Avatar>
            </IconButton>
          </MaterialTooltip>
        </li>

        {/* <li className="horizontalNavItem">
          <SearchRoundedIcon fontSize="500" sx={customStyles.customIcons} />
        </li> */}
      </Grid>
      <Menu
        anchorEl={anchorEl}
        id="account-menu"
        open={open}
        onClose={handleClose}
        onClick={handleClose}
        PaperProps={{
          elevation: 0,
          sx: {
            overflow: "visible",
            filter: "drop-shadow(0px 2px 8px rgba(0,0,0,0.32))",
            mt: 1.5,
            "& .MuiAvatar-root": {
              width: 32,
              height: 32,
              ml: -0.5,
              mr: 1,
            },
            "&:before": {
              content: '""',
              display: "block",
              position: "absolute",
              top: 0,
              right: 14,
              width: 10,
              height: 10,
              bgcolor: "background.paper",
              transform: "translateY(-50%) rotate(45deg)",
              zIndex: 0,
            },
          },
        }}
        transformOrigin={{ horizontal: "right", vertical: "top" }}
        anchorOrigin={{ horizontal: "right", vertical: "bottom" }}
      >
        <MenuItem onClick={handleClose}>
          <NavLink className="link-styl" to="utilisateurs">
            <div className="align">
              <Person fontSize="medium" />
              <h5 style={{ marginLeft: 5 }}>Profile</h5>
            </div>
          </NavLink>
        </MenuItem>
        <Divider />
        <MenuItem onClick={handleClose}>
          <NavLink onClick={handleEditModalOpen} className="link-styl">
            <div className="align">
              <PersonAdd fontSize="medium" />
              <h5 style={{ marginLeft: 5 }}>Ajouter tilisateurs</h5>
            </div>
          </NavLink>
        </MenuItem>
        <Divider />
        <MenuItem>
          <NavLink onClick={handleLogout} className="link-styl">
            <div className="align">
              <LogoutRoundedIcon fontSize="medium" />
              <h5 style={{ marginLeft: 5 }}>Déconnexion</h5>
            </div>
          </NavLink>
        </MenuItem>
      </Menu>
      <EditModal
        handleEdit={handleEditUser}
        open={editModalOpen}
        fields={fields}
        onClose={handleEditModalClose}
        title={"Créer un compte"}
        editedFields={editedFields}
        setEditedFields={setEditedFields}
      />
    </Grid>
  );
};

const mapStateToProps = ({ AuthReducer }) => ({
  transactions: AuthReducer.transactions,
  userInfo: AuthReducer.userInfo,
  loading: AuthReducer.loading,
  userProfile: AuthReducer.userProfile,
  userProjects: AuthReducer.userProjects,
  userGroupes: AuthReducer.userGroupes,
});

export default connect(mapStateToProps)(HorizontalNav);
