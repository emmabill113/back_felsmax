import React from "react";
import { Box, Avatar, Button } from "@mui/material";
import "./NavBar.css";
import { NavLink, useLocation } from "react-router-dom";
import HomeRoundedIcon from '@mui/icons-material/HomeRounded';
import FolderRoundedIcon from '@mui/icons-material/FolderRounded';
import GroupsRoundedIcon from '@mui/icons-material/GroupsRounded';
import PersonRoundedIcon from '@mui/icons-material/PersonRounded';
import Logo from "../../assets/img/logo.png";
import { connect, useDispatch } from 'react-redux';
import { logout } from "../../redux/Auth/actions";
import { useNavigate } from "react-router-dom";
import Card from "../Home/Card";
import MonetizationOnSharpIcon from '@mui/icons-material/MonetizationOnSharp';

const Navbar = ({userProfile}) => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const location = useLocation();


  const RevenuClient = () => {
    let sum = 0;
    const totalAmount = userProfile.reduce((acc, user) => {
      if (user?.balance === "NaN") {
        sum = sum;
      } else {
        acc += parseFloat(user?.balance);
      }
      return sum+acc;
    }, 0);
    
    return { totalAmount: parseFloat(totalAmount) };
  };  
  
  const resultTotalCustomer = RevenuClient();
  console.log('ici revenue des client', resultTotalCustomer)

  return (
    <Box style={{ display: "flex", flexDirection: "column", height: "100%", gap: "48%" }}>
      <div style={{ display: "flex", flexDirection: "column", height: "100%"} } className="">
        <nav>
          <header className="logo">
            <img src={Logo} style={{ height: "50px", width: "160px", marginBottom: "10px" }} alt="Logo" />
          </header>
          <ul className="navContainer">
            <li>
              <NavLink to="home" className="link-style" activeClassName="active-link">
                <div className="align">
                  <HomeRoundedIcon sx={{ marginRight: 1 }} />
                  TABLEAU DE BORD
                </div>
              </NavLink>
            </li>
            <li>
              <NavLink to="projets" className="link-style" color="red" activeClassName="active">
                <div className="align">
                  <FolderRoundedIcon sx={{ marginRight: 1, borderRadius: 20 }} />
                  PROJETS
                </div>
              </NavLink>
            </li>
            <li>
              <NavLink to="tontines" className="link-style" activeClassName="active">
                <div className="align">
                  <GroupsRoundedIcon sx={{ marginRight: 1 }} />
                  TONTINES/GAGES
                </div>
              </NavLink>
            </li>
            <li>
              <NavLink to="utilisateurs" className="link-style" activeClassName="active">
                <div className="align">
                  <PersonRoundedIcon sx={{ marginRight: 1 }} />
                  UTULISATEURS
                </div>
              </NavLink>
            </li>
          </ul>
        </nav>
        
        <div className="logout" style={{width:'13%'}}>
        <Card icon={<MonetizationOnSharpIcon/>} title={'revenu client'} nombre={<p style={{color:'blue'}}>{resultTotalCustomer.totalAmount} XAF</p>} titreTooltip={"Ceci indique le montant total d'espèces dépôsé par des clients "}/>
        <Card icon={<MonetizationOnSharpIcon/>} title={'Commissions'} nombre={<p style={{color:'blue'}}>300 XAF</p>} titreTooltip={"Ceci indique le montant total de commision sur les retrait d'espèce dans une tontine, gage, projet. Il inclut aussi la commision de publication du projet qui s'élève à 10000 XFA "}/>
      </div>
      </div>
     
    </Box>
  );
};

const mapStateToProps = ({ AuthReducer }) => ({
  transactions: AuthReducer.transactions,
  userInfo: AuthReducer.userInfo,
  userProfile: AuthReducer.userProfile,
  userProjects: AuthReducer.userProjects,
  userGroupes: AuthReducer.userGroupes,
});

export default connect(mapStateToProps)(Navbar);
