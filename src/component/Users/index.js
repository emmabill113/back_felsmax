import React, { useId, useState } from 'react';
import { Grid } from '@mui/material';
import UserList from './UserList';
import TransactionModal from './TransactionModal';
import { connect, Connect } from "react-redux";
import './index.css'
 

const UserListPage = ({userProfile, laoding}) => {
  
  const [selectedUserId, setSelectedUserId] = useState(null);
  const [openModal, setOpenModal] = useState(false);

  const handleView = (userId) => {
    console.log('dfdffffff', userId)
    setSelectedUserId(userId);
    setOpenModal(true);
  };

  const handleCloseModal = () => {
    setOpenModal(false);
  };


  const getUserById = (userId) => {
    return userProfile.find(user => user._id === userId);
  };

  const selectedUser = selectedUserId ? getUserById(selectedUserId) : null;

  return (
    <Grid style={{padding:'10px'}} container spacing={2} justifyContent="center">
      <UserList userList={userProfile} handleView={handleView}/>
      <TransactionModal
       operateur={selectedUser?.transactions} openModal={openModal} setOpenModal={setOpenModal} handleCloseModal={handleCloseModal} selectedUser={selectedUser} selectedUserId={selectedUserId}/>
    </Grid>
  );
};


const mapStateToProps = ({ AuthReducer }) => ({
  userProfile: AuthReducer.userProfile,
  laoding: AuthReducer.laoding
})
export default connect(mapStateToProps)(UserListPage);
