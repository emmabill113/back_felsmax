import React from "react";
import {
  Modal,
  Box,
  Typography,
  Button,
} from "@mui/material";

const DeleteConfirmationModal = ({
  open,
  onClose,
  handleDelete,
  title,
  description,
  username,
  solde,
  loading
}) => {
  return (
    <Modal
      open={open}
      onClose={onClose}
      aria-labelledby="delete-modal-title"
      aria-describedby="delete-modal-description"
    >
      <Box
        sx={{
          position: "absolute",
          top: "50%",
          left: "50%",
          transform: "translate(-50%, -50%)",
          bgcolor: "background.paper",
          boxShadow: 24,
          p: 4,
          maxWidth: 400,
          borderRadius: 4,
        }}
      >
        <Typography className="titre-modal" variant="h6" id="delete-modal-title" gutterBottom>
          {title}  <p className="active-item-table"> <span style={{color:"#f74949"}}>{username} </span> avec pour solde : <span style={{color:"#f74949"}}>{solde} XAF</span></p>
        </Typography>
        <Typography className="sous-titre" variant="body1" id="delete-modal-description">
          {description}
        </Typography>
        <Box sx={{ display: "flex", justifyContent: "space-evenly", mt: 2 }}>
          <Button
           className="delete-btn"
            onClick={onClose}
            sx={{ marginRight: 2 }}
            variant="contained"
            color="inherit"
          >
            Annuler
          </Button>
          <Button
          className="delete-btn"
            onClick={handleDelete}
            variant="contained"
            color="error"
          >
            {loading ? "En cour ..." : "Désactiver"}
          </Button>
        </Box>
      </Box>
    </Modal>
  );
};

export default DeleteConfirmationModal;
