import React, { useState } from "react";
import {
  Modal,
  Typography,
  Grid,
  ListItem,
  List,
  TextField,
  ListItemText,
  Avatar,
  ListItemAvatar,
  Pagination,
  IconButton,
  Box,
} from "@mui/material";
import MaterialTooltip from "../Tooltip";
import Orange from "../../assets/img/orange.jpg";
import Eu from "../../assets/img/eu.jpg";
import Mtn from "../../assets/img/mtn.png";
import felsmax from "../../assets/img/logo.png";
import uba from "../../assets/img/UBA.gif";
import yoomee from "../../assets/img/yoomee.jpg";
import Moment from "react-moment";
import InputAdornment from "@mui/material/InputAdornment";
import AddToHomeScreenIcon from "@mui/icons-material/AddToHomeScreen";
import SearchRoundedIcon from "@mui/icons-material/SearchRounded";
import { customStyles } from "../../constant/customStyles";
import InstallMobileIcon from "@mui/icons-material/InstallMobile";
import moment from "moment";
import EditModal from "../Edit";
import { updateBalance } from "../../redux/Auth/actions";
import { connect, useDispatch } from "react-redux";
import empty from '../../assets/img/OIP.jpg'

const TransactionModal = ({
  selectedUserId,
  handleCloseModal,
  openModal,
  loading,
  userGroupes,
  userProjects
}) => {
  const [searchDate, setSearchDate] = useState("");
  const [currentPage, setCurrentPage] = useState(1);
  const [usersPerPage] = useState(10);
  const [editModalOpen, setEditModalOpen] = useState(false);
  const [action, setAction] = useState("");

  const [editedFields, setEditedFields] = useState({
    user: "",
    amount: "",
    action: "",
  });
  const fields = [
    {
      label: "Montant",
      stateKey: "amount",
    },
  ];

  const dispatcher = useDispatch();

  const filteredTransactions = selectedUserId?.transactions?.filter(
    (transaction) => {
      if (searchDate) {
        const formattedDate = moment(transaction?.createdAt).format(
          "DD/MM/YYYY"
        );
        return formattedDate === searchDate;
      }
      return true;
    }
  );

  const indexOfLastUser = currentPage * usersPerPage;
  const indexOfFirstUser = indexOfLastUser - usersPerPage;
  const currentUsers = filteredTransactions?.slice(
    indexOfFirstUser,
    indexOfLastUser
  );

  const handlePageChange = (event, value) => {
    setCurrentPage(value);
  };

  const handleEditModalOpen = (item, action) => {
    setEditModalOpen(true);
    setEditedFields((prevEditFields) => ({
      ...prevEditFields,
      action: action,
      user: item,
    }));

    console.log(" ici compte  a éditer", selectedUserId);
  };

  const handleEditModalClose = () => {
    setEditModalOpen(false);
  };

  const handleEditUser = (action, user) => {
    setEditedFields((prevEditFields) => ({
      ...prevEditFields,
      user: user,
    }));
    console.log("", editedFields);
    dispatcher(updateBalance(editedFields));
    handleEditModalClose();
  };
  return (
    <Grid item xs={12}>
      <Modal
        open={openModal}
        onClose={handleCloseModal}
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <div
          className="Modal-containers"
          style={{ display: "flex", justifyContent: "space-evenly" }}
        >
          <div
            className="bloc-left"
            style={{ flex: "1", alignContent: "center" }}
          >
            <div className="bloc-title">
              <div className="avatar">
                <Avatar
                  sx={{
                    bgcolor: "#FF4F4F",
                    fontFamily: "Lato",
                    height: 110,
                    width: 110,
                  }}
                  src={selectedUserId?.user?.picture}
                />
              </div>
              <Typography className="titre-list" variant="h6" gutterBottom>
                {selectedUserId?.user?.surname} {selectedUserId?.user?.name}
              </Typography>
              <Typography className="titre-list" variant="h6" gutterBottom>
                {selectedUserId?.user?.email}
              </Typography>
            </div>
            <div
              className="bloc-title"
              style={{ paddingRight: 5, marginTop: "15%" }}
            >
              <Typography
                className="titre-list-historiy"
                variant="h5"
                gutterBottom
              >
                INFORMATIONS
              </Typography>

              <Typography className="sous-titre" variant="h6" gutterBottom>
                Téléphone :{" "}
                <span className="bloc-solde">
                  {selectedUserId?.user?.phone}
                </span>
              </Typography>
              <Typography className="sous-titre" variant="h6" gutterBottom>
                Date de naissance :{" "}
                <span className="bloc-solde">
                  <Moment format="DD/MM/YYYY">
                    {selectedUserId?.user?.birthday}
                  </Moment>
                </span>
              </Typography>
              <Typography className="sous-titre" variant="h6" gutterBottom>
                Ville :{" "}
                <span className="bloc-solde">{selectedUserId?.user?.city}</span>{" "}
              </Typography>
              <Typography className="sous-titre" variant="h6" gutterBottom>
                Quatier :{" "}
                <span className="bloc-solde">
                  {selectedUserId?.user?.district}
                </span>
              </Typography>

              <Typography className="sous-titre" variant="h6" gutterBottom>
                Soldde :{" "}
                <span
                  style={{
                    color: selectedUserId?.balance === "0" ? "red" : "green",
                  }}
                  className="bloc-solde"
                >
                  {selectedUserId?.balance} XAF
                </span>{" "}
              </Typography>
            </div>
            <div className="iRemoveDeposit">
              <MaterialTooltip title='Faite attention à cette opération que vous souhaiter éffectuer. Vous serrez responsable en cas de désagrement !'>
              <IconButton
                onClick={() =>
                  handleEditModalOpen(selectedUserId?.user?._id, "INCREMMENT")
                }
                className="icn-item"
              >
                <InstallMobileIcon
                  sx={{
                    width: "20px",
                  }}
                />
                Créditer
              </IconButton>
              </MaterialTooltip>
              <MaterialTooltip title='Faite attention à cette opération que vous souhaiter éffectuer, vous serrez responsable en cas de désagrement !'>
              <IconButton
                onClick={() =>
                  handleEditModalOpen(selectedUserId?.user?._id, "DECREMMENT")
                }
                className={
                  selectedUserId?.balance === "0" ||
                  selectedUserId?.balance === "NaN"
                    ? "desable-icon"
                    : "icn-item"
                }
                disabled={
                  selectedUserId?.balance === "0" ||
                  selectedUserId?.balance === "NaN"
                    ? true
                    : false
                }
              >
                <AddToHomeScreenIcon
                  sx={{
                    width: "20px",
                  }}
                />
                débiter
              </IconButton>
              </MaterialTooltip>
            </div>
          </div>
          <div style={{ flex: "2" }}>
            <List className="table_container-list" style={{ padding: 15 }}>
              <Typography
                className="titre-list-historiy"
                variant="h5"
                gutterBottom
              >
                Historique des transactions
              </Typography>
              <div
                className=""
                style={{
                  display: "flex",
                  marginBottom: 10,
                  marginTop: 10,
                  justifyContent: "right",
                }}
              >
                <TextField
                  type="text"
                  value={searchDate}
                  onChange={(e) => setSearchDate(e.target.value)}
                  className="research"
                  id="outlined-basic"
                  variant="outlined"
                  placeholder="Date de la transaction"
                  sx={customStyles.customFieldStyle}
                  size="small"
                  name="email"
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <SearchRoundedIcon />
                      </InputAdornment>
                    ),
                  }}
                />
              </div>
              {currentUsers && currentUsers?.length > 0 ? (
                currentUsers.map((transaction) => (
                  <TransactionItem
                    transaction={transaction}
                    key={transaction._id}
                  />
                ))
              ) : (
                <Box  sx={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  color: "#f74949",
                  marginTop:'10%'
                }}>
                  <span style={{ color: "#f74949" }} className="titre-list-historiy">pas de transactions à cette date</span>
                  <img style={{marginTop:'15%', width:'150px', height:'150px'}} src={empty} alt="empty"/>
                </Box>
              )}
              <Box
                sx={{
                  display: "flex",
                  justifyContent: "center",
                  marginTop: 4,
                  "& .MuiPaginationItem-root": {
                    color: "inherit",
                    backgroundColor: "inherit",
                    "&.Mui-selected": {
                      color: "white",
                      fontWeight: 600,
                      backgroundColor: "#f74949",
                    },
                  },
                }}
              >
                {filteredTransactions?.length > 0 && (
                  <Pagination
                    count={Math.ceil(
                      filteredTransactions.length / usersPerPage
                    )}
                    page={currentPage}
                    onChange={handlePageChange}
                    shape="circular"
                  />
                )}
              </Box>
            </List>
          </div>
        </div>
      </Modal>
      <EditModal
        onClose={handleEditModalClose}
        setEditedFields={setEditedFields}
        handleEdit={() => handleEditUser(action, selectedUserId?.user?._id)}
        title={
          action === "INCREMMENT" ? "Créditer le compte" : "Débiter le compte"
        }
        sousTilte={selectedUserId?.user?.name + selectedUserId?.user?.surname}
        editedFields={editedFields}
        fields={fields}
        open={editModalOpen}
      />
    </Grid>
  );
};

const TransactionItem = ({ transaction }) => {
  const operatorMap = {
    ORANGE_MONEY: Orange,
    mtn: Mtn,
    FELSMAX: felsmax,
    UBA: uba,
    YOOMEE: yoomee,
    default: Eu,
  };

  const operatorImage =
    operatorMap[transaction?.operator] || operatorMap?.default;

  return (
    <div className="bloc-transaction" key={transaction._id}>
      <ListItem className="listItem">
        <ListItemAvatar>
          <Avatar src={operatorImage} />
        </ListItemAvatar>
        <ListItemText
          primary={<p className="titre-list-item">{transaction.type}</p>}
          secondary={
            <p className="titre-list-item">
              Montant : {transaction.amount + " XAF"}
            </p>
          }
        />
        <ListItemText
          primary={<p className="titre-list-item">ID : {transaction._id}</p>}
          secondary={
            <p className="titre-list-item">Téléphone : {transaction.phone}</p>
          }
        />
        <ListItemText
          primary={
            <p className="titre-list-item">
              <Moment format="DD/MM/YYYY">{transaction.createdAt}</Moment>
            </p>
          }
          secondary={
            <p
              className="titre-list-item"
              style={{
                color: transaction.status === "CANCELED" ? "red" : "#06d6a0",
              }}
            >
              {transaction.status}
            </p>
          }
        />
      </ListItem>
    </div>
  );
};

const mapStateToProps = ({ AuthReducer }) => ({
  loading: AuthReducer.loading,
  userProjects: AuthReducer.userProjects,
  userGroupes: AuthReducer.userGroupes,
});

export default connect(mapStateToProps)(TransactionModal);
