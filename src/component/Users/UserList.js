import React, { useState } from "react";
import {
  IconButton,
  Grid,
  Typography,
  TextField,
  Box,
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  Pagination,
} from "@mui/material";
import InputAdornment from "@mui/material/InputAdornment";
import SearchRoundedIcon from "@mui/icons-material/SearchRounded";
import { Visibility, Delete, Edit } from "@mui/icons-material";
import "./index.css";
import { customStyles } from "../../constant/customStyles";
import DeleteConfirmationModal from "./DeleteModal";
import { desactivedUser } from "../../redux/Auth/actions";
import { connect, useDispatch } from "react-redux";
import EditModal from "../Edit";
import MaterialTooltip from "../Tooltip";
import LoadingButton from "@mui/lab/LoadingButton";


const UserList = ({ userList, handleView, loading }) => {
  const [searchTerm, setSearchTerm] = useState("");
  const [deleteModalOpen, setDeleteModalOpen] = useState(false);
  const [selectedUser, setSelectedUser] = useState(null);
  const dispatch = useDispatch();
  const [currentPage, setCurrentPage] = useState(1);
  const [usersPerPage] = useState(15);
  const [editModalOpen, setEditModalOpen] = useState(false);

  const fields = [
    {
      label: "Nom",
      stateKey: "editedUsername",
    },
    {
      label: "Prenom",
      stateKey: "editedPrenom",
    },
    {
      label: "email",
      stateKey: "editedEmail",
    },
    {
      label: "Téléphone",
      stateKey: "editedPhone",
    },
    {
      label: "Ville",
      stateKey: "editedVille",
    },
    {
      label: "Quatier",
      stateKey: "editedQuatier",
    },
    {
      label: "Mot de passe",
      stateKey: "editedPassword",
    },
    {
      label: "Confirme le mot de passe",
      stateKey: "editedConfirmPass",
    },
    {
      label: "Solde",
      stateKey: "editedSolde",
    },
    // Ajoutez les autres champs ici
  ];

  const [editedFields, setEditedFields] = useState({
    editedUsername: "",
    editedPrenom: "",
    editedSolde: "",
    editedVille: "",
    editedQuatier: "",
    editedEmail: "",
    editedPassword:"",
    editedPhone:"",
    editedConfirmPass:"",

  });

  const handleSearchChange = (e) => {
    setSearchTerm(e.target.value);
  };

  const handleDeleteModalOpen = (userId, userName, lastName, solde) => {
    setSelectedUser({ id: userId, name: userName, surname: lastName, balance: solde });
    setDeleteModalOpen(true);
    console.log("vous avez cliquer : ici l'id a supprimer", userId);
  };

  const handleDeleteModalClose = () => {
    setDeleteModalOpen(false);
  };

  const deleteUser = () => {
    const { id: userId } = selectedUser;
    console.log("vous avez cliquer : ici l'id a supprimer", userId);
    dispatch(desactivedUser(userId));
    handleDeleteModalClose();
  };


  const filteredUserList = userList
  .filter((user) => {
    const name = user?.user?.name?.toLowerCase();
    const surname = user?.user?.surname?.toLowerCase();
    const email = user?.user?.email?.toLowerCase();
    const search = searchTerm.toLowerCase();

    return (
      name?.includes(search) ||
      surname?.includes(search) ||
      email?.includes(search)
    );
  })
  .sort((a, b) => {
    const nameA = a?.user?.name?.toLowerCase();
    const nameB = b?.user?.name?.toLowerCase();
    return nameA.localeCompare(nameB);
  });


  const handleEditModalOpen = (userId,userName, lastName, solde) => {
    setSelectedUser({ id: userId, name: userName, surname: lastName, balance: solde });
    setEditModalOpen(true);

    console.log(" ici compte  a éditer", userName);
  };

  const handleEditModalClose = () => {
    setEditModalOpen(false);
  };

  const handleEditUser = () => {
    // Logique de mise à jour de l'utilisateur
    handleEditModalClose();
  };
  
  // Pagination
  const indexOfLastUser = currentPage * usersPerPage;
  const indexOfFirstUser = indexOfLastUser - usersPerPage;
  const currentUsers = filteredUserList.slice(
    indexOfFirstUser,
    indexOfLastUser
  );

  const handlePageChange = (event, value) => {
    setCurrentPage(value);
  };

  return (
    <Grid item xs={12}>
      <div className="table_container-list-user">
        <Typography className="titre-user" variant="h5" gutterBottom>
          Liste d'utilisateurs
        </Typography>
        <div style={{justifyContent:"right"}} >
      </div>
        <div
          className=""
          style={{
            display: "flex",
            marginBottom: 10,
            marginTop: 10,
            justifyContent: "right",
          }}
        >
          <TextField
            value={searchTerm}
            onChange={handleSearchChange}
            className="research"
            id="outlined-basic"
            variant="outlined"
            placeholder="Email ou nom"
            sx={customStyles.customFieldStyle}
            size="small"
            name="email"
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <SearchRoundedIcon />
                </InputAdornment>
              ),
            }}
          />
        </div>
        <div className="user-table-container">
          <Table className="user-table">
            <TableHead>
              <TableRow className="item-table">
                <TableCell className="item-table">NOM</TableCell>
                <TableCell className="item-table">PRENOM</TableCell>
                <TableCell className="item-table">EMAIL</TableCell>
                <TableCell className="item-table">TELEPHONE</TableCell>
                <TableCell className="item-table">STATUS</TableCell>
                <TableCell className="item-table">ACTIONS</TableCell>
              </TableRow>
            </TableHead>
          </Table>
          <div className="user-table-scroll">
            <Table className="user-table">
              <TableBody className="user-table-body">
                {currentUsers.map((user) => (
                  <TableRow  className="active-item-table email-cell" key={user?._id}>
                    <TableCell className="item_row_table email-cell">{user?.user?.name}</TableCell>
                    <TableCell className="item_row_table email-cell">{user?.user?.surname}</TableCell>
                    <TableCell className="item_row_table email-cell">{user?.user?.email}</TableCell>
                    <TableCell className="item_row_table email-cell">{user?.user?.phone}</TableCell>
                    <TableCell className="item_row_table email-cell">
                      <p
                        className=""
                        style={{
                          color: user?.user?.isActive ? "#57cc99" : "#f74949",
                        }}
                      >
                        {user?.user?.isActive ? "Actif" : "Désactivé"}
                      </p>
                    </TableCell>
                    <TableCell className="item_row_table email-cell">
                      <MaterialTooltip title='Voir plus de détails'>
                      <IconButton onClick={() => handleView(user)}>
                        <Visibility sx={{ width: "18px", color: "#8ecae6" }} />
                      </IconButton>
                      </MaterialTooltip>
                      <MaterialTooltip title='Modifier un utilisateur'>
                      <IconButton  onClick={
                          () =>handleEditModalOpen(
                            user?.user?._id,
                            user?.user?.name,
                            user?.user?.surname,
                            user?.balance
                          )}>
                        <Edit sx={{ width: "18px", color: "#ffb703" }} />
                      </IconButton>
                      </MaterialTooltip>
                      <MaterialTooltip title='Supprimer un utilisateur'>
                      <IconButton
                        onClick={
                          () =>handleDeleteModalOpen(
                            user?.user?._id,
                            user?.user?.name,
                            user?.user?.surname,
                            user?.balance
                          )}
                        disabled={!user?.user?.isActive}
                      >
                        <Delete
                          sx={{
                            width: "18px",
                            color: user?.user?.isActive ? "#f74949" : "#adb5bd",
                          }}
                        />
                      </IconButton>
                      </MaterialTooltip>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </div>
        </div>

        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            marginTop: 2,
            "& .MuiPaginationItem-root": {
              color: "inherit",
              backgroundColor: "inherit",
              "&.Mui-selected": {
                color: "white",
                fontWeight: 600,
                backgroundColor: "#f74949",
              },
            },
          }}
        >
          <Pagination
            count={Math.ceil(filteredUserList.length / usersPerPage)}
            page={currentPage}
            onChange={handlePageChange}
            shape="circular"
          />
        </Box>
      </div>

      {/* Pagination */}

      <DeleteConfirmationModal
        description={"Êtes-vous sûr de vouloir désactiver ce compte ?"}
        title={"CONFIRMATION DE DESACTIVATION DU COMPTE"}
        username={selectedUser?.name + ' '+''+ selectedUser?.surname}
        solde={selectedUser?.balance}
        handleDelete={deleteUser}
        open={deleteModalOpen}
        loading={loading}
        onClose={handleDeleteModalClose}
      />
      <EditModal
         handleEdit={handleEditUser} 
         open={editModalOpen}
         fields={fields}
         onClose={handleEditModalClose}
         title={'Editer le compte'}
         username={selectedUser?.surname}
         editedFields={editedFields}
         setEditedFields={setEditedFields}
         />
    </Grid>
  );
};

const mapStateToProps = ({ AuthReducer }) => ({
  userProfile: AuthReducer.userProfile,
  loading: AuthReducer.loading,
});

export default connect(mapStateToProps)(UserList);
