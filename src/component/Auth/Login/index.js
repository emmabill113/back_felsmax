import * as React from "react";
import {

  Box,
  Button,
  TextField,
  Grid,
  Divider,
} from "@mui/material";
import LoadingButton from '@mui/lab/LoadingButton';
import { useNavigate } from "react-router-dom";
import "./Login.css";
import { customStyles } from "../../../constant/customStyles"
import InputAdornment from '@mui/material/InputAdornment';
import { connect, useDispatch } from 'react-redux';
import LockIcon from '@mui/icons-material/Lock';
import EmailIcon from '@mui/icons-material/Email';
import GoogleIcon from '@mui/icons-material/Google';
import FacebookIcon from '@mui/icons-material/Facebook';
import { useState, useEffect } from "react";
import { login } from "../../../redux/Auth/actions"

const Login = ({ loading, status, isLogged }) => {

  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [formData, setFormData] = useState({
    email: '',
    password: '',
  });
  const [errors, setErrors] = useState({
    emailMessage: null,
    passwordMessage: null,
  })
  const [loader, setLoader] = useState(loading)

  useEffect(() => {
    console.log(loader)
    if (isLogged == true) {
      navigate('/Dashboard/home');
    }
    else {
      navigate('/')
    }
  }, [isLogged])



  const checkEmailValidity = (email) => {
    const emailFormat = new RegExp(/^[A-Za-z0-9_!#$%&'*+\/=?`{|}~^.-]+@[A-Za-z0-9.-]+$/, "gm");
    return emailFormat.test(email);
  }
  const checkPasswordValidity = (password) => {
    return password.length >= 8
  }
  const checkFieldsValidity = () => {
    if (formData.email === '' && formData.password === '') {
      setErrors({
        emailMessage: 'Veillez entrer une addresse mail.',
        passwordMessage: 'Veillez entrer un password'
      });
      return false
    } else if (formData.email === '') {
      setErrors({
        ...errors,
        emailMessage: 'Veillez entrer une addresse mail.',
      })
      return false
    } else if (formData.password === '') {
      setErrors({
        ...errors,
        passwordMessage: 'Password'
      })
      return false
    }
    return true;
  }

  const handleChange = (e) => {
    const val = e.target.value;
    switch (e.target.name) {
      case 'email':
        setFormData({ ...formData, email: val });
        break;
      case 'password':
        setFormData({ ...formData, password: val });
        break;
    }
  }

  const handleLogin = () => {
    if (checkEmailValidity(formData.email) && checkPasswordValidity(formData.password) && checkFieldsValidity()) {
      console.log("data : ", formData.email, formData.password);
    }
    setLoader(true)
    console.log("valeur de logged: ", isLogged)
    dispatch(login(formData))
    console.log(status)
    if (isLogged == true && loading == false) {
      console.log("valeur de logged: ", isLogged)
      navigate('/Dashboard/home');
    }
    else {
      setLoader(false)
      navigate('/')
    
    }

  }

  const fieldsValidity = () => {
    const emailRegex = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$/;
  
    if (
      formData.email === "" ||
      formData.password === "" ||
      formData.password.length < 6 ||
      !emailRegex.test(formData.email)
    ) {
      return true;
    }
    return false;
  };
  


  return (
    <Box className="container">
      <Box className="left-card"></Box>
      <Box className="right-card">
        <Grid className="body-container">
          <Grid>
            <p className="titre-login">CONNEXION</p>
          </Grid>
          <Grid className="login-fields">
            <TextField
              className="loginInput"
              id="outlined-basic"
              variant="outlined"
              placeholder="Entrer votre mail"
              size="medium"
              name='email'
              value={formData.email}
              onChange={handleChange}
              error={errors.emailMessage !== null}
              sx={customStyles.customFieldStyle}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <EmailIcon color="action" />
                  </InputAdornment>
                ),
              }}
            />

          </Grid>
          <Grid className="login-fields">
            <TextField
              id="outlined-basic"
              className="loginInput"
              variant="outlined"
              placeholder="Votre mot de passe"
              size="medium"
              name='password'
              sx={customStyles.customFieldStyle}
              value={formData.password}
              onChange={handleChange}
              error={errors.passwordMessage !== null}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <LockIcon color="action" />
                  </InputAdornment>
                ),
              }}
            />
          </Grid>
          <Grid className="forget">Mot de passe oublié ?</Grid>
          <LoadingButton
            loading = {loading}
            loadingPosition="start"
            color={fieldsValidity() ? "inherit" : "error"}
             size="medium" 
             variant="contained" 
             className="sign-up-btn loginBtn" 
             onClick={handleLogin}
             disabled={fieldsValidity()}
          >
           {loading?"Connexion...":"Se connecter"}
          </LoadingButton>
          <Grid className="Divider">
            <Divider><p className="text">ou</p></Divider>
          </Grid>
          <Grid className="social-btn">
            <Button variant="outlined" className="social" startIcon={<GoogleIcon color="error"/>} >Google</Button>
            <Button variant="outlined" className="social" startIcon={<FacebookIcon color="info" />}>Facebook</Button>
          </Grid>
        </Grid>
      </Box>
    </Box>
  );
};
const mapStateToProps = ({ AuthReducer }) => ({
  loading: AuthReducer.loading,
  status: AuthReducer.status,
  isLogged: AuthReducer.isLogged,

})
export default connect(mapStateToProps)(Login);
