import React from "react";
import {
  Grid,
  List,
  ListItem,
  ListItemAvatar,
  Avatar,
  ListItemText,
  Typography,
  Divider,
} from "@mui/material";
import "./index.css";

const ListGroupProject = ({ item, vue }) => {
  // Vérifier si l'élément a un solde non nul
  const hasPositiveBalance = (element) => {
    return (
      element.withdrawalAmount > 0 || element.amount > 0 || element.balance >0
    );
  };

  // Filtrer les éléments ayant un solde positif et trier par ordre décroissant de solde
  const sortedItems = item
    .filter(hasPositiveBalance)
    .sort((a, b) => b.withdrawalAmount - a.withdrawalAmount || b.balance - a.balance );

    

  // Obtenir les cinq premiers éléments de la liste triée
  const topFiveItems = sortedItems.slice(0, 5);

  console.log('ddddddddddd', topFiveItems )

  return (
    <Grid className="chart bottomChart">
      <div className="bottomChartTitle">
        {vue === "groupe" ? (
          <h5 className="text1">Groupes les plus rentables</h5>
        ) : vue === "projet" ? (
          <h5 className="text1">Projets les plus rentables</h5>
        ) : (
          <h5 className="text1">Comptes les plus importants</h5>
        )}
      </div>
      <div>
        <List
          sx={{ width: "100%", maxWidth: 360, bgcolor: "background.paper" }}
        >
          {topFiveItems.map((items, index) => {
            const hasName = (items.name || items.title) || items?.balance;
            const hasWithdrawalAmount = ( items.withdrawalAmount || items.amount) || items?._id;

            if (hasName && hasWithdrawalAmount) {
              return (
                <React.Fragment key={index}>
                  <ListItem style={{ padding: 5 }} alignItems="flex-start">
                    <ListItemAvatar>
                      <Avatar alt={items.name} src={items.avatar} />
                    </ListItemAvatar>
                    <ListItemText
                      primary={
                        <React.Fragment>
                          <div
                            style={{
                              display: "flex",
                              flexDirection: "row",
                              alignItems: "center",
                              justifyContent: "space-between",
                            }}
                          >
                            <Typography
                              className="titre-list-item"
                              sx={{ display: "inline" }}
                              component="span"
                              variant="body2"
                              color="text.primary"
                            >
                              {items?.name || items?.title || items?.user?.name}
                            </Typography>
                            <Typography
                              className="titre-list-item"
                              sx={{ display: "inline" }}
                              component="span"
                              variant="body2"
                              color="text.primary"
                            >
                              {vue === "groupe" ? "Type :" : vue==="projet"? "Domaine :":" "}
                              {vue === "groupe"? items?.accountTypes : vue==="projet"? items?.domaine : items?.user?.email}
                            </Typography>
                          </div>
                        </React.Fragment>
                      }
                      secondary={
                        <React.Fragment>
                          <div
                            style={{
                              display: "flex",
                              flexDirection: "row",
                              alignItems: "center",
                              justifyContent: "space-between",
                            }}
                          >
                            <Typography
                              className="titre-list-item"
                              sx={{ display: "inline" }}
                              component="span"
                              variant="body2"
                              color="text.primary"
                            >
                              Solde :{" "}
                              <span className="span-info">
                                {" "}
                                {items?.withdrawalAmount ||
                                  items?.amount ||
                                  items?.balance}{" "}
                                XAF
                              </span>
                            </Typography>
                            <Typography
                              className="titre-list-item"
                              sx={{ display: "inline" }}
                              component="span"
                              variant="body2"
                              color="text.primary"
                            >
                              {(vue==="groupe" || vue==="projet")?'Membres : ':""}
                              {vue === "groupe"? items?.members?.length : vue==="projet"?
                                items?.memberId?.length : items?.user?.phone}
                            </Typography>
                          </div>
                        </React.Fragment>
                      }
                    />
                  </ListItem>
                  {index !== topFiveItems.length - 1 && (
                    <Divider variant="inset" component="li" />
                  )}
                </React.Fragment>
              );
            }
            return null;
          })}
        </List>
      </div>
    </Grid>
  );
};

export default ListGroupProject;
