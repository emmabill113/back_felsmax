import React, { useState } from "react";
import {
  Modal,
  Typography,
  Grid,
  ListItem,
  List,
  TextField,
  ListItemText,
  Avatar,
  ListItemAvatar,
  Box,
} from "@mui/material";
import Orange from "../../assets/img/orange.jpg";
import EU from "../../assets/img/eu.jpg";
import Mtn from "../../assets/img/mtn.png";
import felsmax from "../../assets/img/logo.png";
import uba from "../../assets/img/UBA.gif";
import Moment from "react-moment";
import InputAdornment from "@mui/material/InputAdornment";
import SearchRoundedIcon from "@mui/icons-material/SearchRounded";
import { customStyles } from "../../constant/customStyles";
import moment from "moment";
import empty from "../../assets/img/OIP.jpg";
import MemberList from "./MemberList";
import GroupIcon from "@mui/icons-material/Group";
import FolderSharpIcon from "@mui/icons-material/FolderSharp";
import DoneAllSharpIcon from "@mui/icons-material/DoneAllSharp";
import Resume from "./Resume";
import MaterialTooltip from "../Tooltip";
import { useDispatch, connect } from "react-redux";
import { publishProject } from "../../redux/Auth/actions";
import LoadingButton from "@mui/lab/LoadingButton";

const TransactionModalProjetGroup = ({
  selectItemId,
  handleCloseModal,
  selectItem,
  vue,
  loading,
  status,
  message,
}) => {
  const [searchDate, setSearchDate] = useState("");
  const dispatch = useDispatch();
  console.log('message', message);
  const filteredTransactions = selectItem?.cotisations?.filter(
    (transaction) => {
      if (searchDate) {
        const formattedDate = moment(transaction?.createdAt).format(
          "DD/MM/YYYY"
        );
        return formattedDate === searchDate;
      }
      return true;
    }
  );

  const [open, setOpen] = useState(true);

  const handleClick = () => {
    setOpen(!open);
  };

  const publierProjet = () => {
    const id = selectItemId;
    console.log("vous aller publier le projet", id);
    dispatch(publishProject(id));
    handleClick();
  };

  return (
    <Grid item xs={12}>
      <Modal
        open={selectItemId !== null}
        onClose={handleCloseModal}
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <div
          className="Modal-containers"
          style={{ display: "flex", justifyContent: "space-evenly" }}
        >
          <div
            className="bloc-left"
            style={{ flex: "1", alignContent: "center" }}
          >
            <div className="bloc-title">
              <div className="avatar">
                <Avatar
                  sx={{
                    bgcolor: "#FF4F4F",
                    fontFamily: "Lato",
                    height: 110,
                    width: 110,
                  }}
                  src={selectItem?.user?.picture}
                />
              </div>
              <Typography className="titre-list" variant="h6" gutterBottom>
                {selectItem?.name || selectItem?.title}
              </Typography>
              <Typography className="titre-list" variant="h6" gutterBottom>
                {selectItem?.user?.email}
              </Typography>
            </div>
            <div style={{ paddingRight: 5 }}>
              {vue === "groupe" ? (
                <Typography className="sous-titre" variant="h6" gutterBottom>
                  Fréquence :{" "}
                  <span className="bloc-solde">
                    {selectItem?.frequence + " jours" || ""}
                  </span>
                </Typography>
              ) : (
                ""
              )}
              <Typography className="sous-titre" variant="h6" gutterBottom>
                Objectif :{" "}
                <span className="bloc-solde">
                  {selectItem?.contributionAmount} XAF
                </span>{" "}
              </Typography>
              <Typography className="sous-titre" variant="h6" gutterBottom>
                Solde :{" "}
                <span className="bloc-solde">
                  {selectItem?.withdrawalAmount} XAF
                </span>{" "}
              </Typography>
            </div>
            <div>
              {vue === "groupe" ? (
                ""
              ) : (
                <MemberList
                  icon={<FolderSharpIcon />}
                  tilte={"Documents"}
                  items={selectItem?.files}
                />
              )}
            </div>
            <MemberList
              icon={<GroupIcon />}
              tilte={vue === "groupe" ? "Liste membres" : "Participants"}
              items={selectItem?.members || selectItem?.memberId}
            />
            {vue === "groupe" ? (
              ""
            ) : (
              <Resume
                tilte={"Description"}
                icon={<DoneAllSharpIcon />}
                resume={selectItem?.resume}
              />
            )}
            {vue === "groupe" || selectItem?.status === "Approved" ? (
              ""
            ) : (
              <MaterialTooltip title="En cliquant ici vous allaz publier ce projet et sera visible par tous les utilisateur Felsmax">
                <LoadingButton
                  loadingPosition="start"
                  loading={loading}
                  style={{
                    borderRadius: "12px",
                    marginTop: 20,
                    height: 32,
                    textTransform:loading?'lowercase':'uppercase',
                    width:loading?130:85,
                  }}
                  className="btn-publish"
                  onClick={publierProjet}
                >
                  {loading?"En cours...":"Publier"}
                </LoadingButton>
              </MaterialTooltip>
            )}
            {message && loading && <p className="msg">{message}</p>}
          </div>
          <div style={{ flex: "2" }}>
            <List className="table_container-list" style={{ padding: 15 }}>
              <Typography
                className="titre-list-historiy"
                variant="h5"
                gutterBottom
              >
                Historique des transactions
              </Typography>
              <div
                className=""
                style={{
                  display: "flex",
                  marginBottom: 10,
                  marginTop: 10,
                  justifyContent: "right",
                }}
              >
                <TextField
                  type="text"
                  value={searchDate}
                  onChange={(e) => setSearchDate(e.target.value)}
                  className="research"
                  id="outlined-basic"
                  variant="outlined"
                  placeholder="Date de la transaction"
                  sx={customStyles.customFieldStyle}
                  size="small"
                  name="email"
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <SearchRoundedIcon />
                      </InputAdornment>
                    ),
                  }}
                />
              </div>
              {filteredTransactions && filteredTransactions.length > 0 ? (
                filteredTransactions.map((transaction) => (
                  <TransactionItem
                    transaction={transaction}
                    key={transaction._id}
                  />
                ))
              ) : (
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center",
                    color: "#f74949",
                    marginTop: "10%",
                  }}
                >
                  <span
                    style={{ color: "#f74949" }}
                    className="titre-list-historiy"
                  >
                    pas de transactions à cette date
                  </span>
                  <img
                    style={{
                      marginTop: "15%",
                      width: "150px",
                      height: "150px",
                    }}
                    src={empty}
                    alt="empty"
                  />
                </Box>
              )}
            </List>
          </div>
        </div>
      </Modal>
    </Grid>
  );
};

const TransactionItem = ({ transaction }) => {
  const operatorMap = {
    orange: Orange,
    mtn: Mtn,
    FELSMAX: felsmax,
    UBA: uba,
    default: EU,
  };

  const operatorImage =
    operatorMap[transaction.operator] || operatorMap.default;

  return (
    <div className="bloc-transaction" key={transaction._id}>
      <ListItem className="listItem">
        <ListItemAvatar>
          <Avatar src={operatorImage} />
        </ListItemAvatar>
        <ListItemText
          primary={<p className="titre-list-item">{transaction.type}</p>}
          secondary={
            <p className="titre-list-item">
              Montant : {transaction.amount + " XAF"}
            </p>
          }
        />
        <ListItemText
          primary={<p className="titre-list-item">ID : {transaction._id}</p>}
          secondary={
            <p className="titre-list-item">Téléphone : {transaction.phone}</p>
          }
        />
        <ListItemText
          primary={
            <p className="titre-list-item">
              <Moment format="DD/MM/YYYY">{transaction.createdAt}</Moment>
            </p>
          }
          secondary={
            <p
              className="titre-list-item"
              style={{
                color: transaction.status === "CANCELED" ? "red" : "#06d6a0",
              }}
            >
              {transaction.status}
            </p>
          }
        />
      </ListItem>
    </div>
  );
};
const mapStateToProps = ({ AuthReducer }) => ({
  loading: AuthReducer.loading,
  status: AuthReducer.status,
  message: AuthReducer.message,
});
export default connect(mapStateToProps)(TransactionModalProjetGroup);
