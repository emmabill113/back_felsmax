import React from "react";
import {
  Modal,
  Box,
  Typography,
  Button,
} from "@mui/material";

const DeleteConfirmationModal = ({
  open,
  onClose,
  handleDelete,
  title,
  description,
  vue
}) => {
  return (
    <Modal
      open={open}
      onClose={onClose}
      aria-labelledby="delete-modal-title"
      aria-describedby="delete-modal-description"
    >
      <Box
        sx={{
          position: "absolute",
          top: "50%",
          left: "50%",
          transform: "translate(-50%, -50%)",
          bgcolor: "background.paper",
          boxShadow: 24,
          p: 4,
          maxWidth: 400,
          borderRadius: 4,
        }}
      >
        <Typography className="titre-modal" variant="h5" id="delete-modal-title" gutterBottom>
          {title}
        </Typography>
        <Typography className="sous-titre" variant="body1" id="delete-modal-description">
          {description}
        </Typography>
        <Box sx={{ display: "flex", justifyContent: "space-evenly", mt: 2 }}>
          <Button
           className="titre-btn"
            onClick={onClose}
            sx={{ marginRight: 2 }}
            variant="contained"
            color="info"
          >
            Annuler
          </Button>
          <Button
          className="titre-btn"
            onClick={handleDelete}
            variant="contained"
            color="error"
          >
            Supprimer
          </Button>
        </Box>
      </Box>
    </Modal>
  );
};

export default DeleteConfirmationModal;
