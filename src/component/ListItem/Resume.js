import React, { useState } from 'react';
import { Collapse, List, ListItemButton, ListItemIcon, Typography, Avatar, IconButton } from '@mui/material';
import { ExpandLess, ExpandMore, Delete } from '@mui/icons-material';
import './index.css'

const Resume = ({resume, icon,tilte}) => {
  const [open, setOpen] = useState(false);

  const handleClick = () => {
    setOpen(!open);
  };

  return (
    <div>
      <ListItemButton onClick={handleClick}>
        <ListItemIcon style={{color:open?'#f74949':""}}>
          {icon}
       </ListItemIcon>
        <Typography style={{color:open?"#f74949":""}} className="titre-list">{tilte}</Typography>
        {open ? <ExpandLess style={{color:open?"#f74949":""}} /> : <ExpandMore />}
      </ListItemButton>
      <Collapse style={{ maxHeight: '150px', overflow: 'auto' }} in={open} timeout="auto" unmountOnExit>
        <Typography style={{textAlign:'justify', padding:5}} className='titre-lts'>
            {resume}
        </Typography>
      </Collapse>
    </div>
  );
};

export default Resume;
