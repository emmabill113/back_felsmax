import React, { useState } from 'react';
import { Collapse, List, ListItemButton, ListItemIcon, Typography, Avatar, IconButton } from '@mui/material';
import { ExpandLess, ExpandMore, Delete } from '@mui/icons-material';

const MemberList = ({ items,tilte, icon }) => {
  const [open, setOpen] = useState(false);

  const handleClick = () => {
    setOpen(!open);
  };

  return (
    <div>
      <ListItemButton onClick={handleClick}>
        <ListItemIcon style={{color:open?'#f74949':""}}>
          {icon}
       </ListItemIcon>
        <Typography style={{color:open?'#f74949':""}} className="titre-list">{tilte}</Typography>
        {open ? <ExpandLess style={{color:open?'#f74949':""}} /> : <ExpandMore />}
      </ListItemButton>
      <Collapse style={{ maxHeight: '150px', overflow: 'auto' }} in={open} timeout="auto" unmountOnExit>
        <List disablePadding>
          {items?.map((member, index) => (
            (tilte==='Liste membres' || tilte==='Participants')?
            <ListItemButton style={{ height: '40px', marginTop: 10 }} key={index} sx={{ pl: 2 }}>
              <Avatar sx={{ bgcolor: "gray", fontFamily: "Lato", height: 25, width: 25 }} src="" />
              <Typography className="titre-list" style={{ marginLeft: 10 }}>
                {`${member?.name} ${member?.surname}`}
              </Typography>
              <IconButton onClick={() => console.log("Supprimer le membre !")}>
                <Delete sx={{ width: "18px", color: "#f74949" }} />
              </IconButton>
            </ListItemButton>
            :
            <a href={member} target="_blank" rel="noopener noreferrer" style={{marginTop: 10 , textDecoration:"none"}} key={index} sx={{ pl: 2 }}>
              <Typography className="titre-list" style={{ marginLeft: 10 }}>
                {`Fichier ${index+1}`}
              </Typography>
            </a>
          ))}
        </List>
      </Collapse>
    </div>
  );
};

export default MemberList;
