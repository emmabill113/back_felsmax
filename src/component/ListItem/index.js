import React, { useState } from "react";
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  IconButton,
  Grid,
  Typography,
  TextField,
  CircularProgress,
  Pagination,
  Box,
  Select,
  MenuItem,
  Tooltip
} from "@mui/material";
import InputAdornment from "@mui/material/InputAdornment";
import SearchRoundedIcon from "@mui/icons-material/SearchRounded";
import AddCircleIcon from '@mui/icons-material/AddCircle';
import { Visibility, Delete, Edit } from "@mui/icons-material";
import { customStyles } from "../../constant/customStyles";
import DeleteConfirmationModal from "./GroupProjetDelete";
import EditModal from "../Edit";
import empty from "../../assets/img/OIP.jpg";
import MaterialTooltip from "../Tooltip";
import { connect } from "react-redux";


const ListItemProjetGroup = ({
  itemList,
  handleView,
  vue,
  options,
  loading,
}) => {
  const [searchTerm, setSearchTerm] = useState("");
  const [deleteModalOpen, setDeleteModalOpen] = useState(false);
  const [selectItem, setselectItem] = useState(null);
  const [currentPage, setCurrentPage] = useState(1);
  const [itemPerPage] = useState(10);
  const [editModalOpen, setEditModalOpen] = useState(false);
  const [searchTermTwo, setSearchTwo] = useState("tous");

  const fields = [
    {
      label: "Titre",
      stateKey: "editedTitre",
    },
    {
      label: "Date de debut",
      stateKey: "editedPrenom",
    },
    {
      label: "Date de fin",
      stateKey: "editedEmail",
    },
    {
      label: "Domaine",
      stateKey: "editedPhone",
    },
    {
      label: "Secteur",
      stateKey: "editedVille",
    },
    {
      label: "Quatier",
      stateKey: "editedQuatier",
    },
    {
      label: "Mot de passe",
      stateKey: "editedPassword",
    },
    {
      label: "Confirme le mot de passe",
      stateKey: "editedConfirmPass",
    },
    {
      label: "Solde",
      stateKey: "editedSolde",
    },
    // Ajoutez les autres champs ici
  ];

  const [editedFields, setEditedFields] = useState({
    editedUsername: "",
    editedPrenom: "",
    editedSolde: "",
    editedVille: "",
    editedQuatier: "",
    editedEmail: "",
    editedPassword: "",
    editedPhone: "",
    editedConfirmPass: "",
  });

  const handleSearchChange = (e) => {
    setSearchTerm(e.target.value);
  };

  const handleDeleteModalOpen = (user) => {
    setselectItem(user);
    setDeleteModalOpen(true);
    console.log("dfdfdfdfd", selectItem);
  };

  const handleDeleteModalClose = () => {
    setDeleteModalOpen(false);
  };

  const deleteUser = () => {
    const userId = selectItem;
    console.log("vous avez cliquer : ici l'id a supprimer", userId);
    handleDeleteModalClose();
  };

  const filtereditemList = itemList
    ?.filter((item) => {
      const name = item?.name?.toLowerCase();
      const title = item?.title?.toLowerCase();
      const status = item?.status?.toLowerCase();
      const typeAccount = item?.accountTypes?.toLowerCase();
      const search = searchTerm?.toLowerCase();
      const searchTwo = searchTermTwo?.toLowerCase();

      if (searchTermTwo === "tous")
        return name?.includes(search) || title?.includes(search);

      return (
        (name?.includes(search) || title?.includes(search)) &&
        (status?.includes(searchTwo) || typeAccount?.includes(searchTwo))
      );
    })
    ?.sort((a, b) => {
      const nameA =
        a.name?.toLowerCase() ||
        a.title?.toLowerCase() ||
        a.status?.toLowerCase() ||
        a.typeAccount?.toLowerCase();
      const nameB =
        b.name?.toLowerCase() ||
        b.title?.toLowerCase() ||
        b.typeAccount?.toLowerCase() ||
        b.status?.toLowerCase();

      return nameA?.localeCompare(nameB);
    });

  const indexOfLastUser = currentPage * itemPerPage;
  const indexOfFirstUser = indexOfLastUser - itemPerPage;
  const currentUsers = filtereditemList?.slice(
    indexOfFirstUser,
    indexOfLastUser
  );

  const handlePageChange = (event, value) => {
    setCurrentPage(value);
  };

  const handleEditModalOpen = (itemId) => {
    setselectItem({
      id: itemId,
    });
    setEditModalOpen(true);

    console.log(" ici compte  a éditer", itemId);
  };

  const handleEditModalClose = () => {
    setEditModalOpen(false);
  };

  const handleEditUser = () => {
    // Logique de mise à jour de l'utilisateur
    handleEditModalClose();
  };

  const handleVueChange = (event) => {
    setSearchTwo(event.target.value);
    console.log("iiiiiiiii", searchTermTwo);
  };

  console.log('loaaaad', loading)
  return (
    <Grid item xs={12}>
      <div className="table_container-list-user">
        <Typography className="titre-user" variant="h5" gutterBottom>
          {vue === "groupes" ? "Liste de groupes" : "Liste de projets"}
        </Typography>
        <div
          style={{
            display: "flex",
            marginBottom: 10,
            marginTop: 10,
            justifyContent: "right",
          }}
        >
          {loading && <CircularProgress size={24} />}

          <div> 
            <MaterialTooltip  title={vue==="groupes"?'Nouveau groupe':"Nouveau projet"}>
            <IconButton style={{marginRight:15}} onClick={() => console.log('ajouter un truc ici')}>
              <AddCircleIcon  sx={{fontSize:25, color: "#219ebc" }} />
            </IconButton>
            </MaterialTooltip>
            <Select
              value={searchTermTwo}
              onChange={handleVueChange}
              className="research"
              variant="outlined"
              // placeholder="Sélectionnez une option"
              sx={customStyles.customFieldStyle}
              size="small"
            >
              {options.map((option) => (
                <MenuItem
                  className="titre-list"
                  key={option.value}
                  value={option.value}
                >
                  {option.label}
                </MenuItem>
              ))}
            </Select>
            <TextField
              value={searchTerm}
              onChange={handleSearchChange}
              className="research"
              id="outlined-basic"
              variant="outlined"
              placeholder={
                vue === "groupes" ? "Nom du groupe" : "Nom du projet"
              }
              sx={customStyles.customFieldStyle}
              size="small"
              name="email"
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <SearchRoundedIcon />
                  </InputAdornment>
                ),
              }}
            />
          </div>
        </div>
        <div className="user-table-container">
          <Table className="user-table">
            <TableHead>
              <TableRow className="item-table">
                <TableCell className="item-table">NOM</TableCell>
                <TableCell className="item-table">MEMBRES</TableCell>
                {vue === "groupes" ? (
                  <TableCell className="item-table">TYPE</TableCell>
                ) : (
                  ""
                )}
                {vue === "groupes" ? (
                  <TableCell className="item-table">FREQUENCE</TableCell>
                ) : (
                  <TableCell className="item-table">STATUS</TableCell>
                )}
                <TableCell className="item-table">ACTIONS</TableCell>
              </TableRow>
            </TableHead>
          </Table>
          <div className="user-table-scroll">
            <Table className="user-table">
              <TableBody className="user-table-body">
                {currentUsers && currentUsers.length > 0 ? (
                  currentUsers?.map((item) => (
                    <TableRow className="" key={item?._id}>
                      <TableCell className="item_row_table">
                        {item?.name || item?.title}
                      </TableCell>
                      <TableCell className="item_row_table">
                        {item?.members?.length || item?.membreId?.length}
                      </TableCell>
                      {vue === "groupes" && (
                        <TableCell className="item_row_table">
                          {item?.accountTypes}
                        </TableCell>
                      )}
                      <TableCell>
                        <p
                          className="item_row_table"
                          style={{
                            color:
                              item?.status === "Approved" ? "#f74949" : "black",
                          }}
                        >
                          {item?.status === "Submitted"
                            ? "Soumis"
                            : "Approuvé" || item?.frequence + " Jours"}
                        </p>
                      </TableCell>
                      <TableCell>
                        <MaterialTooltip title={vue==="groupes"?'Voir les détails du groupe':"Voir les détails du projet"}>
                        <IconButton onClick={() => handleView(item?._id)}>
                          <Visibility
                            sx={{ width: "18px", color: "#8ecae6" }}
                          />
                        </IconButton>
                        </MaterialTooltip>
                        <MaterialTooltip title={vue==="groupes"?'Modifier le groupe':"Modifier le projet"}>
                        <IconButton
                          onClick={() => handleEditModalOpen(item._id)}
                        >
                          <Edit sx={{ width: "18px", color: "#ffb703" }} />
                        </IconButton>
                        </MaterialTooltip>
                        <MaterialTooltip title={vue==="groupes"?'Supprimer le groupe':"Supprimer le projet"}>
                        <IconButton
                          onClick={() => handleDeleteModalOpen(item?._id)}
                        >
                          <Delete
                            sx={{
                              width: "18px",
                              color: "#f74949",
                            }}
                          />
                        </IconButton>
                        </MaterialTooltip>
                      </TableCell>
                    </TableRow>
                  ))
                ) : (
                  <Box
                    sx={{
                      display: "flex",
                      flexDirection: "column",
                      alignItems: "center",
                      color: "#f74949",
                      marginTop: "10%",
                    }}
                  >
                    <span
                      style={{ color: "#f74949" }}
                      className="titre-list-historiy"
                    >
                      Acun résultat trouvé !
                    </span>
                    <img
                      style={{
                        marginTop: "10%",
                        width: "100px",
                        height: "100px",
                      }}
                      src={empty}
                      alt="empty"
                    />
                  </Box>
                )}
              </TableBody>
            </Table>
          </div>
        </div>

        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            marginTop: 2,
            "& .MuiPaginationItem-root": {
              color: "inherit",
              backgroundColor: "inherit",
              "&.Mui-selected": {
                color: "white",
                fontWeight: 600,
                backgroundColor: "#f74949",
              },
            },
          }}
        >
          <Pagination
            count={Math.ceil(filtereditemList?.length / itemPerPage)}
            page={currentPage}
            onChange={handlePageChange}
            shape="circular"
          />
        </Box>
      </div>

      {/* Pagination */}

      <DeleteConfirmationModal
        description={"Êtes-vous sûr de vouloir supprimer cet utilisateur ?"}
        title={"Confirmation de suppression du compte ID " + selectItem}
        handleDelete={deleteUser}
        open={deleteModalOpen}
        loading={loading}
        onClose={handleDeleteModalClose}
      />

      <EditModal
        handleEdit={handleEditUser}
        open={editModalOpen}
        fields={fields}
        onClose={handleEditModalClose}
        title={"Editer le projet"}
        editedFields={editedFields}
        setEditedFields={setEditedFields}
      />
    </Grid>
  );
};

const mapStateToProps = ({ AuthReducer }) => ({
  loading : AuthReducer.loading,
});
export default connect(mapStateToProps)(ListItemProjetGroup);
