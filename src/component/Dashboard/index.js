import * as React from "react";
import { Box, Grid, Avatar } from "@mui/material";
import "./style.css";

import Navbar from "../NavBar";
import HorizontalNav from "../HorizontalNav";
import Home from "../Home";
import Projet from "../Projet"
import { BrowserRouter as Router, Route, Routes, Outlet} from "react-router-dom";

const Dashboard = () => {
  return (
    
      <Box className="container">
      <Grid className="navbar">
        <Navbar />
      </Grid>
      <Grid className="centerPage">
        <Grid className="dashboard">
           <HorizontalNav /> 
        </Grid>
        <Grid className="childComponent" id="detail">
          <Outlet />
        </Grid>
      </Grid>
    </Box>

  );
};
export default Dashboard;
