import React, { PureComponent } from 'react';
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';

const LinearChart = ({userProjects,userGroupes}) => {


    const groupTransactions = () => {
        const groupedData = {};
      
        const currentYear = new Date().getFullYear(); // Obtient l'année en cours
      
        userProjects.forEach(project => {
          const date = new Date(project.createdAt);
          const year = date.getFullYear(); // Obtient l'année du projet
          const month = date.toLocaleString('default', { month: 'long' });
      
          // Vérifie si le projet appartient à l'année en cours
          if (year === currentYear) {
            if (!groupedData[month]) {
              groupedData[month] = {
                projects: 0,
                groups: 0,
              };
            }
      
            groupedData[month].projects += 1; // Comptage du nombre de projets
          }
        });
      
        userGroupes.forEach(groupe => {
          const date = new Date(groupe.createdAt);
          const year = date.getFullYear(); // Obtient l'année du groupe
          const month = date.toLocaleString('default', { month: 'long' });
      
          // Vérifie si le groupe appartient à l'année en cours
          if (year === currentYear) {
            if (!groupedData[month]) {
              groupedData[month] = {
                projects: 0,
                groups: 0,
              };
            }
      
            groupedData[month].groups += 1; // Comptage du nombre de groupes
          }
        });
      
        const sortedMonths = Object.keys(groupedData).sort((a, b) => {
          const monthA = new Date(`${a} 1, ${currentYear}`);
          const monthB = new Date(`${b} 1, ${currentYear}`);
          return monthA - monthB;
        });
      
        const chartData = sortedMonths.map(month => ({
          month,
          PROJETS: groupedData[month].projects,
          GROUPES: groupedData[month].groups,
        }));
      
        return chartData;
      };
      
      const data = groupTransactions();

  return (
    <>
      <div className='chartTitle'>
        <p style={{ fontWeight: "bolder", fontSize: "18px", marginTop: 0, marginBottom: 0 }}>
          <strong>Evolution du nombre de groupes et de projets</strong>
        </p>
        <p style={{ marginTop: 2, fontSize: "14px" }}>Classé par mois</p>
      </div>
      <ResponsiveContainer width="100%" height="80%">
        <LineChart
          width={500}
          height={300}
          data={data}
          margin={{
            top: 5,
            right: 30,
            left: 20,
            bottom: 5,
          }}
        >
          <CartesianGrid strokeDasharray="" />
          <XAxis dataKey="month" />
          <YAxis />
          <Tooltip contentStyle={{borderRadius:15}}/>
          <Legend />
          <Line type="monotone" dataKey="PROJETS" stroke="#fb6107" activeDot={{ r: 8 }} />
          <Line type="monotone" dataKey="GROUPES" stroke="#104911" />
        </LineChart>
      </ResponsiveContainer>
    </>
  );
}

export default LinearChart;