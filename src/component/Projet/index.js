import React, { useState } from 'react';
import { Grid } from '@mui/material';
import ListItemProjetGroup from '../ListItem';
import TransactionModalProjetGroup from '../ListItem/GroupProjectTransactions';
import { connect} from "react-redux";
import './index.css'
 

const Projet = ({userProjects, laoding}) => {
  console.log('ici la liste des groupe', userProjects)
  
  const [selectItemId, setselectItemId] = useState(null);

  const handleView = (userId) => {
    setselectItemId(userId);
  };

  const handleCloseModal = () => {
    setselectItemId(null);
  };

  const option= [
    {
      value: 'tous', label: 'Tous'
    }, 
    {
      value: 'Submitted', label: 'Soumi(s)'
    }, 
    {
      value: 'Approved', label: 'Approuvé(s)'
    }, 
  ]

  const getUserById = (userId) => {
    return userProjects.find(user => user._id === userId);
  };

  const selectItem = selectItemId ? getUserById(selectItemId) : null;

  return (
    <Grid style={{padding:'10px'}} container spacing={2} justifyContent="center">
      <ListItemProjetGroup options={option} vue={''} itemList={userProjects} handleView={handleView}/>
      <TransactionModalProjetGroup  handleCloseModal={handleCloseModal} selectItem={selectItem} selectItemId={selectItemId}/>
    </Grid>
  );
};


const mapStateToProps = ({ AuthReducer }) => ({
  userProjects: AuthReducer.userProjects,
  laoding: AuthReducer.laoding
})
export default connect(mapStateToProps)(Projet);
