import React from "react";
 import { styled } from '@mui/material/styles';
import Tooltip, { tooltipClasses } from '@mui/material/Tooltip';

 const MaterialTooltip = styled(({ className, ...props }) => (
    <Tooltip {...props} arrow classes={{ popper: className }} />
  ))(({ theme }) => ({
    [`& .${tooltipClasses.arrow}`]: {
      color:"#219ebc",
    },
    [`& .${tooltipClasses.tooltip}`]: {
      backgroundColor: "#219ebc",
      color: "white",
      fontSize:'12px',
      fontFamily: 'Quicksand, sans-serif'
    },
  }));

export default MaterialTooltip;