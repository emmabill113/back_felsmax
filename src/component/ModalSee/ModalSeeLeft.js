import React from 'react';
import { Grid, Avatar, Chip, List, ListItemButton, ListItemIcon, ListItemText, Collapse } from '@mui/material';
import ExpandLess from '@mui/icons-material/ExpandLess';
import PersonIcon from '@mui/icons-material/Person';
import ExpandMore from '@mui/icons-material/ExpandMore';
import StarBorder from '@mui/icons-material/StarBorder';
import './ModalSee.css';
import { Height } from '@mui/icons-material';

const ModalSeeLeft = ({ vue, nom, auteur, nbMembres, secteur, domaine, montant, resume, statut, membres }) => {

  const [open, setOpen] = React.useState(false);
  const handleClick = () => {
    setOpen(!open);
  };

  return (
    <Grid className="modalheader">
      <div className='avatar'>
        <Avatar sx={{ bgcolor: "#FF4F4F", fontFamily: "Lato", height: 130, width: 130 }}>N</Avatar>
      </div>
      <div className='information'>
        <p className="title">{vue === 'projet' ? 'Nom du projet:' : 'Nom du groupe:'} <strong className="lBody_text">{nom}</strong></p>
        <p className="title">{vue === 'projet' ? 'Autheur du projet:' : 'Autheur du groupe:'} <strong className="lBody_text">{auteur}</strong></p>
        <p className="title">Nombre de membre: <strong className="lBody_text">{nbMembres}</strong></p>
        {vue === 'projet' && <p className="title">Secteur : <strong className="lBody_text">{secteur}</strong></p>}
        {vue === 'projet' && <p className="title">Domaine : <strong className="lBody_text">{domaine}</strong></p>}
        {vue === 'projet' && <p className="title">Objectif : <strong className="lBody_text">{montant} XAF</strong></p>}
        {vue === 'projet' && <p className="title">Statut  : <Chip label="Approuvé" size="small" color="success" style={{ marginLeft: '20px', fontSize: 10, fontWeight: "bold" }} /></p>}
        {vue !== 'projet' && <p className="title">Objectif : <strong className="lBody_text">{montant}</strong></p>}
        {vue !== 'projet' && <p className="title">Solde : <strong className="lBody_text">{resume} XAF</strong></p>}
        {membres && membres.length > 0 && (
          <List component="nav" aria-labelledby="nested-list-subheader" className="member-list">
            <ListItemButton onClick={handleClick}>
              <ListItemText primary="Membres" />
              {open ? <ExpandLess /> : <ExpandMore />}
            </ListItemButton>
            <Collapse in={open} timeout="auto" unmountOnExit>
              <List className='list_member'   component="div" disablePadding>
                {membres.map((membre, index) => (
                  <ListItemButton key={index} sx={{ pl: 4 }}>
                    <ListItemIcon>
                      <PersonIcon />
                    </ListItemIcon>
                    <ListItemText primary={membre.name} />
                  </ListItemButton>
                ))}
              </List>
            </Collapse>
          </List>
        )}
      </div>
    </Grid>
  );
};

export default ModalSeeLeft;