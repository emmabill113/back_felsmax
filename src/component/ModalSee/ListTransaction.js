import React from 'react';
import { Grid, Divider } from '@mui/material';
import PersonIcon from '@mui/icons-material/Person';
import SwapVertRoundedIcon from '@mui/icons-material/SwapVertRounded';

const TransactionList = ({ transactions, vue }) => {
    
    console.log(transactions, 'jjjjjjjjjjjjjjjjjjjjj')

    return (
        <Grid className='right_body mbody'>
            <p style={{ fontFamily: "Lato", fontSize: 19 }}>Liste des transactions : </p>
            <Divider variant="middle" color="grey" />
            {transactions?.length > 0 ? (
                transactions.map((transaction, index) => (
                    <div key={index}>
                        <Grid>
                            <p>{transaction.type}:</p>
                            <Grid className='Transaction'>
                                <div className="transaction_body">
                                    <PersonIcon sx={{ marginTop: 1, paddingLeft: 1, color: "grey" }} />
                                    <p>{transaction.name}</p>
                                </div>
                                <div className="transaction_body">
                                    <SwapVertRoundedIcon sx={{ marginTop: 1, color: transaction.type === 'Depot' ? "green" : "red" }} />
                                    <p>{transaction.amount}</p>
                                </div>
                            </Grid>
                        </Grid>
                        <Divider variant="middle" color="grey" />
                    </div>
                ))
            ) : (
                <p>Pas de transaction</p>
            )}
        </Grid>
    );
};

export default TransactionList;