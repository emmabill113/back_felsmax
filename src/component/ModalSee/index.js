import * as React from 'react';
import { Box, Grid, Button, Avatar, Divider, Chip } from "@mui/material";
import Modal from '@mui/material/Modal';
import Backdrop from '@mui/material/Backdrop';
import "./ModalSee.css";
import SwapVertRoundedIcon from "@mui/icons-material/SwapVertRounded";
import DeleteRoundedIcon from '@mui/icons-material/DeleteRounded';
import PersonIcon from '@mui/icons-material/Person';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { red } from '@mui/material/colors';
import ModalSeeLeft from './ModalSeeLeft';
import TransactionList from './ListTransaction'
const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: "60%",
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4,
    height: "80%",
};


const transactionsProjet = [
    { type: 'Depot', name: 'Dava Emmanuel', amount: 45000 },
    { type: 'Depot', name: 'Dava Emmanuel', amount: 45000 },
    { type: 'Depot', name: 'Dava Emmanuel', amount: 45000 },
];

const transactionsTontine = [
    { type: 'UBA', name: 'Dava Emmanuel', amount: 45000 },
    { type: 'Depot', name: 'Dava Emmanuel', amount: 45000 },
    { type: 'Depot', name: 'Dava Emmanuel', amount: 45000 },
];

const membres = ['Membre 1', 'Membre 2', 'Membre 3'];



const ModalSee = ({ open, onClose, type, vue, data }) => {
    const [openSee, setOpenSee] = React.useState(open);
    console.log(data, 'hhhhhhhhh')
    return (

        <>
            {type == "SEE" ? (
                <Modal
                    aria-labelledby="transition-modal-title"
                    aria-describedby="transition-modal-description"
                    open={open}
                    onClose={onClose}
                    closeAfterTransition
                    BackdropComponent={Backdrop}
                    BackdropProps={{
                        timeout: 500,

                    }}>
                    <Box sx={style} className="modal_container">
                        <ModalSeeLeft nom={data?.Nom} nbMembres={data?.Nombre} montant={data?.Account}  membres={data?.Membres} secteur={data?.Secteur} vue={vue} domaine={data?.Domaine} />
                        <TransactionList vue={vue === 'projet' ? 'projet' : 'tontine'} transactions={vue === 'projet' ? transactionsProjet : transactionsTontine} />
                    </Box>
                </Modal>
            ) : (
                <Dialog
                    open={open}
                    onClose={onClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                    className="DialogContainer"
                >
                    <DialogTitle id="alert-dialog-title" className="titleDialog">
                        <DeleteRoundedIcon sx={{ color: red[500], marginTop: 0.5, marginRight: 1 }} />
                        {vue==="projet" ? "Suppression de projet" : "Suppresion du Projet"}
                    </DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description">
                            Confirmer la suppression du projet : <strong>{data?.Nom}</strong>
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions className="dialogActions">
                        <Button onClick={onClose} variant="outlined" autoFocus className="dialogButton">
                            Annuler
                        </Button>
                        <Button variant="contained" autoFocus color="error" startIcon={<DeleteRoundedIcon />} className="dialogButton" onClick={onClose}>
                            Supprimer
                        </Button>

                    </DialogActions>
                </Dialog>
            )}
        </>
    );

}

export default ModalSee;