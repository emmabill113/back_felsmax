import React from "react";
import { Grid, IconButton } from "@mui/material";
import './Home.css'
import '../../component/Users/index.css'
import MaterialTooltip from "../Tooltip";
import InfoIcon from '@mui/icons-material/Info';

const Card = ({ title, icon, nombre, progressIcon, progressPercentage, titreTooltip }) => {
  return (
    <Grid className="first-card card">
      <div className="titleCard">
        <div className="part1">
          <div className="iconBorder card1">
            {icon}
          </div>
          <p className="titre-card">{title}</p>
        </div>
      </div>
      <div className="part2">
        <div className="revenu">
          {nombre}
        </div>
        <div className="iconProgress">
          {progressIcon}
          <p>{progressPercentage}</p>
          <MaterialTooltip  title={titreTooltip}>
            <IconButton style={{marginRight:15, width:10, height:10}} onClick={() => console.log('ajouter un truc ici')}>
              <InfoIcon  sx={{fontSize:15, color: "#219ebc" }} />
            </IconButton>
            </MaterialTooltip>
        </div>
      </div>
    </Grid>
  );
};
export default Card;
