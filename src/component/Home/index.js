import { Box, Grid } from "@mui/material";
import * as React from "react";
import "./Home.css";
import { useEffect } from "react";
import PersonRoundedIcon from "@mui/icons-material/PersonRounded";
import { connect, useDispatch } from "react-redux";
import { getUserProfile } from "../../redux/Auth/actions";
import { red,  green } from "@mui/material/colors";
import MovingRoundedIcon from "@mui/icons-material/MovingRounded";
import ReceiptRoundedIcon from "@mui/icons-material/ReceiptRounded";
import FolderRoundedIcon from "@mui/icons-material/FolderRounded";
import GroupsRoundedIcon from "@mui/icons-material/GroupsRounded";
import BarCharts from "../BarCharts";
import LineChart from "../LineChart";
import ListGroupProject from "../ListGroupeProject";
import Card from "./Card";

const Home = ({ userProjects, userProfile, userInfo, userGroupes }) => {

  const dispatch = useDispatch();
  const [projects, setProject] = React.useState([]);
  useEffect(() => {
    dispatch(getUserProfile());
    setProject(userProjects);
    console.log("projets: ", projects);
    console.log("info: ", userInfo);
    console.log("profile: ", userProfile);
    console.log("groupes: ", userGroupes);
  }, []);

  const getMonthItems = (items, month) => {
    return items?.filter((item) => {
      const itemMonth = new Date(item.createdAt).getMonth();
      return itemMonth === month;
    });
  };

  const calculateTotalTransactions = () => {
    const allTransactions = userProfile.flatMap((user) => user.transactions);
    const total = allTransactions.length;
    console.log("Total Transactions:", total);
    console.log("Toutes les transactions:", allTransactions);
    return { total, allTransactions };
  };

  const RevenuClient = () => {
    let sum = 0;
    const totalAmount = userProfile.reduce((acc, user) => {
      if (user?.balance === "NaN") {
        sum = sum;
      } else {
        acc += parseFloat(user?.balance);
      }
      return sum+acc;
    }, 0);
    
    return { totalAmount: parseFloat(totalAmount) };
  };  
  
  const resultTotalCustomer = RevenuClient();
  console.log('ici revenue des client', resultTotalCustomer)

  
  const currentMonth = new Date().getMonth();
  const monthProjects = getMonthItems(userProjects, currentMonth).length;
  const monthGroupes = getMonthItems(userGroupes, currentMonth).length;
  const monthUsers = getMonthItems(userProfile, currentMonth).length;
  const allTrans = userProfile.flatMap((user) => user.transactions);
  const monthTrans = getMonthItems(allTrans, currentMonth).length;

  console.log('toute les transaction de juin', monthTrans)

  const { total, allTransactions } = calculateTotalTransactions();
  console.log("Total des transactions:", total);
  console.log("Toutes les transactions:", allTransactions);


  return (
    <Box className="containers">
      <Box style={{ width: "100%" }}>
        <Box className="card-list">
          <Card
            title={"Total utilisateurs"}
            titreTooltip={'Total utilisateur créé ce mois'}
            icon={
              <PersonRoundedIcon
                sx={{ height: 20, width: 20, color: green[500] }}
              />
            }
            progressIcon={<MovingRoundedIcon sx={{ color: "green" }} />}
            nombre={userProfile.length}
            progressPercentage={monthUsers}
          />
          <Card
            title={"Total transactions"}
            titreTooltip={'Total transaction du mois'}
            icon={
              <ReceiptRoundedIcon
                sx={{ height: 20, width: 20, color: red[500] }}
              />
            }
            progressIcon={<MovingRoundedIcon sx={{ color: "green" }} />}
            nombre={total}
            progressPercentage={monthTrans}
          />
          <Card
            title={"Total projets"}
            titreTooltip={'Total de projet créé(s) ce mois'}
            icon={
              <FolderRoundedIcon
                sx={{ height: 20, width: 20, color: red[500] }}
              />
            }
            progressIcon={<MovingRoundedIcon sx={{ color: "green" }} />}
            nombre={userProjects.length}
            progressPercentage={monthProjects}
          />
          <Card
            title={"Total groupes"}
            titreTooltip={'Total de groupe créé(s) ce mois'}
            icon={
              <GroupsRoundedIcon
                sx={{ height: 20, width: 20, color: red[500] }}
              />
            }
            progressIcon={<MovingRoundedIcon sx={{ color: "green" }} />}
            nombre={userGroupes.length}
            progressPercentage={monthGroupes}
          />
        </Box>
        <Box className="bodyComponent">
          <Grid className="chartContainer">
            <Grid className=" chart">
              <BarCharts transactions={allTransactions} />
            </Grid>
            <Grid className=" chart">
              <LineChart userGroupes={userGroupes} userProjects={userProjects} />
            </Grid>
            <ListGroupProject vue={"groupe"} item={userGroupes} />
            <ListGroupProject vue={"projet"} item={userProjects} />
            <ListGroupProject item={userProfile} />
          </Grid>
        </Box>
      </Box>
    </Box>
  );
};
const mapStateToProps = ({ AuthReducer }) => ({
  transactions: AuthReducer.transactions,
  userInfo: AuthReducer.userInfo,
  userProfile: AuthReducer.userProfile,
  userProjects: AuthReducer.userProjects,
  userGroupes: AuthReducer.userGroupes,
});
export default connect(mapStateToProps)(Home);
