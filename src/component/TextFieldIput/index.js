import React from "react";
import { Grid, TextField } from "@mui/material";
import { customStyles } from "../../constant/customStyles";

const renderTextField = (label, value, onChange, titre, length) => (
  <Grid item style={{marginTop:10, with:'100%'}} xs={12/length}>
          <TextField
            value={value}
            onChange={onChange}
            id="outlined-basic"
            variant="outlined"
            sx={customStyles.customFieldStyle}
            placeholder={label}
            defaultValue={value}
            size="small"
            fullWidth

          />
  </Grid>
);

export default renderTextField;