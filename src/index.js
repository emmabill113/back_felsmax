import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import Dashboard from "./component/Dashboard";
import Home from "./component/Home"
import Projet from "./component/Projet"
import Login from "./component/Auth/Login"
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react';
import { store, persistor } from './redux/setup'
import Tontine from './component/Tontine';
import UserListPage from './component/Users';
import CreatUserFrorm from './component/Users/CreateUser';

import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";
const router = createBrowserRouter([
  {
    path: "/",
    element: <Login />
  },
  {
    path: "/Dashboard",
    element: <Dashboard />,
    children: [
      {
        path: "home",
        element: <Home />
      },
      {
        path: "projets",
        element: <Projet />
      },
      
      {
        path: "tontines",
        element: <Tontine />
      },
      {
        path: "utilisateurs",
        element: <UserListPage/>
      },
      {
        path: "createuser",
        element: <CreatUserFrorm/>
      }

    ]
  }
])

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <RouterProvider router={router} />
      </PersistGate>
    </Provider>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
