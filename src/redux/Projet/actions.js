import * as types from './types'

export const getProjet = payload => ({
    type: types.GET_PROJET_REQUEST,
    payload: payload
})
