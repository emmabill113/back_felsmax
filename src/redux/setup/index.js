import createSagaMiddleware from '@redux-saga/core';
import { persistStore } from 'redux-persist';
import { persistedReducer } from './reducers';
import { applyMiddleware, createStore } from 'redux';
import Sagas from './saga';


// Saga middleware
const sagaMiddleWare = createSagaMiddleware();

export const store = createStore(
    persistedReducer,
    applyMiddleware(sagaMiddleWare),
);

export const persistor = persistStore(store); 

// Run the saga
sagaMiddleWare.run(Sagas);