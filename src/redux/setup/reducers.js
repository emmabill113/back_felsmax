import { combineReducers } from 'redux';
import { all } from 'redux-saga/effects';
import persistConfig from './persistConfig';
import AuthReducer from '../Auth/reducers';
import { persistReducer } from 'redux-persist';

const rootReducer = combineReducers ({
    AuthReducer
});
export const persistedReducer = persistReducer(persistConfig, rootReducer) 