import {all} from 'redux-saga/effects';
import AuthSaga from '../Auth/saga';

export default function* Sagas() {
    yield all([AuthSaga()]);
  }