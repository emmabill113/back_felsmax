import *  as types from './types'

export const login = payload =>({
    type: types.LOGIN_REQUEST,
     payload
});

export const logout = payload => ({
    type: types.LOGOUT_REQUEST,
    payload
})

export const getUserProfile = ()=>({
    type: types.GET_USER_PROFILE_REQUEST,
})

export const desactivedUser = payload => ({
    type: types.DESACTIVED_USER_PROFILE_REQUEST,
    payload
})


export const register = (payload) => ({
    type: types.SIGN_UP_REQUEST,
    payload
  });
  
  export const updateBalance = (payload) => ({
    type: types.UPDATE_BALANCE_REQUEST,
    payload
  });


  export const publishProject = (payload) => ({
    type: types.PUBLISH_PROJECT_REQUEST,
    payload
  });
  

