import { fabClasses } from "@mui/material";
import * as types from "./types";

const INITIAL_STATE = {
  loading: false,
  status: null,
  errorMsg: "",
  loggedMessage: "",
  message:"",
  isLogged: null,
  userProjects: [],
  userInfo: null,
  transactions: [],
  userProfile: null,
  userGroupes: [],
};

function AuthReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case types.LOGIN_REQUEST:
      return {
        ...state,
        loading: true,
        status: false,
      };
    case types.LOGIN_FAILURE:
      return {
        ...state,
        loading: false,
        status: false,
        errorMsg: action.payload,
        isLogged: false,
      };
    case types.LOGIN_SUCCESS:
      return {
        ...state,
        // loading: false,
        status: action.payload.statut,
        loggedMessage: action.loggedMessage,
        isLogged: true,
      };
    case types.LOGOUT_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case types.LOGOUT_SUCCESS:
      return {
        ...state,
        isLogged: false,
        loading: false,
        userInfo: null,
      };
    case types.GET_USER_PROFILE_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case types.GET_USER_PROFILE_FAILURE:
      return {
        ...state,
        loading: false,
      };
    case types.GET_USER_PROFILE_SUCCESS:
      const info = action.payload.data;
      const projets = action.payload.projects;
      const details = action.payload.AdminAccount;
      console.log("les details de ladmin", details);

      return {
        ...state,
        loading: false,
        userProfile: info,
        userInfo: { ...details },
        transactions: action.payload.data.transactions,
        userProjects: projets,
        userGroupes: action.payload.groupe,
      };

    case types.DESACTIVED_USER_PROFILE_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case types.DESACTIVED_USER_PROFILE_FAILURE:
      return {
        ...state,
        loading: false,
      };
    case types.DESACTIVED_USER_PROFILE_SUCCESS:
      const infos = action.payload.data;
      const projet = action.payload.projects;
      const detail = action.payload.data.user;

      return {
        ...state,
        loading: false,
        userProfile: infos,
        userInfo: detail,
        transactions: action.payload.data.transactions,
        userProjects: projet,
        userGroupes: action.payload.groupe,
      };
    case types.SIGN_UP_REQUEST:
      return {
        ...state,
        loading: true,
        status: null,
        message: "",
        errorMsg: "",
      };
    case types.SIGN_UP_FAILURE:
      return {
        ...state,
        loading: false,
        errorMsg: action?.payload,
        status: action?.payload?.status,
        message: "",
      };
    case types.SIGN_UP_SUCCESS:
      return {
        ...state,
        loading: false,
        message: action.payload.response.message,
        errorMsg: "",
        status: action.payload.status,
      };

      case types.UPDATE_BALANCE_REQUEST:
      return {
        ...state,
        loading: true,
        status: null,
        message: "",
        errorMsg: "",
      };
    case types.UPDATE_BALANCE_FAILURE:
      return {
        ...state,
        loading: false,
        errorMsg: action?.payload,
        status: action?.payload?.status,
        message: "",
      };
    case types.UPDATE_BALANCE_SUCCESS:
      return {
        ...state,
        loading: false,
        message: action.payload.response.message,
        errorMsg: "",
        status: action.payload.status,
      };
      case types.PUBLISH_PROJECT_REQUEST:
      return {
        ...state,
        loading: true,
        status: null,
        message: "",
        errorMsg: "",
      };
    case types.PUBLISH_PROJECT_FAILURE:
      return {
        ...state,
        loading: false,
        errorMsg: action?.payload,
        status: action?.payload?.status,
        message: "",
      };
    case types.PUBLISH_PROJECT_SUCCESS:
      return {
        ...state,
        loading: false,
        message: action.payload.message,
        errorMsg: "",
        status: action.payload.status,
      };
    default:
      return state;
  }
}

export default AuthReducer;