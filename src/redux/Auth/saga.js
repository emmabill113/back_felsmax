import { put, takeLatest } from 'redux-saga/effects';
import { postRequest, getRequest, postUnauthRequest, putRequest, putRequestFormData } from '../../helpers/api';
import { BASE_URL } from '../../constant/Urls';
import { Navigate } from 'react-router-dom';

import * as types from './types';
import { register } from './actions';

function* login({ payload }) {
  const loginData = {
    email: payload.email,
    password: payload.password
  };

  try {
    const data = yield postUnauthRequest(`${BASE_URL}/user/login`, loginData);
    console.log('ici les data', data);
    if (data.statut === true) {
      yield put({
        type: types.LOGIN_SUCCESS,
        payload: data,
      });
      localStorage.setItem('@user', JSON.stringify(data.response));
      yield getUserProfile(data.response.access_token);
    } else {
      yield put({
        type: types.LOGIN_FAILURE,
        payload: data?.error?.message || data?.errors[0]?.msg
      });
    }
  } catch (err) {
    console.error('Some Error: ', err);
    yield put({
      type: types.LOGIN_FAILURE,
      payload: err,
    });
  }
}

function* logout() {
  localStorage.removeItem('@user');
  localStorage.removeItem('data');
  console.log("logout");
  yield put({ type: types.LOGOUT_SUCCESS });
}

function* getUserProfile() {
  try {
    const data = yield getRequest(`${BASE_URL}/user/profile`);
    if (data.status === true) {
      console.log("user Data ", data);
      yield put({
        type: types.GET_USER_PROFILE_SUCCESS,
        payload: data.response,
      });
      localStorage.setItem('data', JSON.stringify(data));
    } else {
      yield put({
        type: types.GET_USER_PROFILE_FAILURE,
        payload: data.error?.message
      });
      localStorage.removeItem('@user_token');
    }
  } catch (error) {
    console.error('Some Error: ', error);
  }
}

function* signUp({ payload }) {
  let signupData = {
    name: payload.name,
    email: payload.email,
    surname: payload.surname,
    phone: payload.phone,
    birthday: payload.birthday,
    city: payload.city,
    district: payload.district,
    password: payload.password,
  }

  try {
    const data = yield postUnauthRequest(`${BASE_URL}/user/register`, signupData);
    if (data.status === true) {
      console.log('my data of register',data);
      yield getUserProfile(); // Mettre à jour les données utilisateur après la désactivation
      yield put({
        type: types.SIGN_UP_SUCCESS,
        payload: data,
      });
    } else {

      yield put({
        type: types.SIGN_UP_FAILURE,
        payload: data?.error?.message || data?.errors[0]?.msg,
      });
    }
  } catch (error) {
    console.error('Something went wrong...', error);
  }
}

function* desactivedUser({ payload }) {
  try {
    const data = yield postRequest(`${BASE_URL}/user/desactivateUser?id=${payload}`);
    if (data.status === true) {
      console.log("message ", data);
      yield getUserProfile(); // Mettre à jour les données utilisateur après la désactivation
      yield put({
        type: types.DESACTIVED_USER_PROFILE_SUCCESS,
        payload: data.response,
      });
      localStorage.setItem('data', JSON.stringify(data));
    } else {
      yield put({
        type: types.DESACTIVED_USER_PROFILE_FAILURE,
        payload: data.error?.message
      });
    }
  } catch (error) {
    console.error('Some Error: ', error);
  }
}

function* updateBalance({ payload }) {
  let updateData = {
    user: payload.user,
    amount: payload.amount,
    action: payload.action,
  }

  try {

    const data = yield postRequest(`${BASE_URL}/user/account/updateUserAccount/`, JSON.stringify(updateData));
    if (data.success === true) {
      console.log('ici la response', data);
      yield put({
        type: types.UPDATE_BALANCE_SUCCESS,
        payload: data,
      });
      localStorage.setItem('data', JSON.stringify(data))
    } else {
      
      yield put({
        type: types.UPDATE_BALANCE_FAILURE,
        payload: data.data?.msg 
      });
    }
  } catch (error) {
    console.error('Something went wrong...', error);
  }
}

function* publishProject ({ payload }) {
console.log("ici l'id du projet à publier", payload)
  try {
    const data = yield postRequest(`${BASE_URL}/user/project/publishProject?id=${payload}`);
    if (data.success === true) {
      console.log("message ", data);
      yield getUserProfile(); 
      yield put({
        type: types.PUBLISH_PROJECT_SUCCESS,
        payload: data,
      });
      localStorage.setItem('data', JSON.stringify(data));
    } else {
      yield put({
        type: types.PUBLISH_PROJECT_FAILURE,
        payload: data.error?.message
      });
    }
  } catch (error) {
    console.error('Some Error: ', error);
  }
}


export default function* AuthSaga() {
  yield takeLatest(types.LOGIN_REQUEST, login);
  yield takeLatest(types.LOGOUT_REQUEST, logout);
  yield takeLatest(types.GET_USER_PROFILE_REQUEST, getUserProfile);
  yield takeLatest(types.DESACTIVED_USER_PROFILE_REQUEST, desactivedUser);
  yield takeLatest(types.SIGN_UP_REQUEST, signUp);
  yield takeLatest(types.UPDATE_BALANCE_REQUEST, updateBalance);
  yield takeLatest(types.PUBLISH_PROJECT_REQUEST, publishProject);
}
