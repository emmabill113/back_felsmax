import {colors} from "./colors"
import { styled } from '@mui/material/styles';
import Badge from '@mui/material/Badge';
export const customStyles ={
    customFieldStyle: {
        '& label.Mui-focused': {
            color: "red",
        },
        '& .MuiOutlinedInput-root': {
            '& fieldset':{
                borderColor: colors.borderColor,
                borderRadius: 4,
                fontFamily: "auto",
                color:"black"
            },
            '&:hover fieldset': {
              borderRadius: 4,
              borderColor: '#80bdff',
              color: "black",
  
              },
            '&:focused fieldset': {
              borderRadius: 4,
              borderColor: '#80bdff',
              boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
            },
            '&:selected fieldset': {
              borderColor: '#80bdff',
              boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
            },
          '& .MuiInputBase-input': {      
            // Use the system font instead of the default Roboto font.
            fontFamily: [
              '-apple-system',
              'BlinkMacSystemFont',
              '"Segoe UI"',
              'Roboto',
              '"Helvetica Neue"',
              'Arial',
              'sans-serif',
              '"Apple Color Emoji"',
              '"Segoe UI Emoji"',
              '"Segoe UI Symbol"',
            ].join(',')
            
          },
           
        },
    },
    custumButtonStyles: {
        '& MuiButtonBase-root':{
            borderColor: colors.borderColor,
        },
    customLi: {
      marginLeft:2, 
      color:"#333333",
      marginTop: 5,
    },
    customIcons: {
      color: "pink", 
      fontSize:30, 
      margin:0
    } 
    },
    customCardIcons: {
      fontSize: 120,
      color: colors.borderColor
    },

}
export const StyledBadge = styled(Badge)(({ theme }) => ({
    '& .MuiBadge-badge': {
      backgroundColor: '#ff4f4f',
      color: '#ff4f4f',
      boxShadow: `0 0 0 2px ${theme.palette.background.paper}`,
      '&::after': {
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        borderRadius: '50%',
        animation: 'ripple 1.2s infinite ease-in-out',
        border: '1px solid currentColor',
        content: '""',
      },
    },
    '@keyframes ripple': {
      '0%': {
        transform: 'scale(.8)',
        opacity: 1,
      },
      '100%': {
        transform: 'scale(2.4)',
        opacity: 0,
      },
    },
  }));
  