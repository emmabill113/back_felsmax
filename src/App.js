import logo from "./logo.svg";
import "./App.css";

import Dashboard from "./component/Dashboard";
import Home from "./component/Home"
import NavBar from "./component/NavBar"

import { Provider } from 'react-redux'
import { store } from  './redux/setup'
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";


function App() {
  return (
    <Router>
      <div className="App">
       <Dashboard /> 
      </div> 
    </Router>
  );
}

export default App;
