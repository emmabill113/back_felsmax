import Dashboard from "../component/Dashboard"

export const HomeWrappedWithDashboard = (Component) => {
    return <Dashboard ChildComponent={<Component />} />;
};
