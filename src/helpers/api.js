
const json = `application/json`;
const multPart = `multipart/form-data`;

const request = async (contentType, method, url, data, options = {}) => {
    try {
        const user_tokens = localStorage.getItem("@user");
        console.log("user tokens: ",user_tokens)
        const jsonUser = JSON.parse(user_tokens);
        const token = jsonUser?.access_token;
        console.log("tokens: ",token)

        console.log(token)
        const response = await fetch(url, {
            method: method,
            headers: {
                'Content-Type': contentType,
                'Authorization': 'Bearer ' + token,
            },
            body: data,
            ...options
        });
        let jsonResponse = await response.json();
        if (jsonResponse.message == "jwt expired" ||
            jsonResponse.message == "Invalid Access Token" ||
            jsonResponse.message == "Access Token has expired") {
            
            try {
                localStorage.removeItem("@user");
            } catch (error) { }
            return jsonResponse;
        }
        return jsonResponse;
    } catch (error) {
        console.log(error);
    }
};

const unAuthRequest = async (contentType, method, url, data) => {
    const response = await fetch(url, {
        method: method,
        headers: {
            'Content-Type': contentType
        },
        body: JSON.stringify(data)
    })
    if (response)
        return response.json();
    throw new Error("Api call failed");
};

// request Auth
export const getRequest = (url) => request(json, 'GET', url);
export const getFRequest = (url, data) => request(json, 'GET', url, data);
export const postRequest = (url, data) => request(json, 'POST', url, data);
export const putRequest = (url, data) => request(json, 'PUT', url, data);
export const deleteRequest = (url, data) => request(json, 'DELETE', url, data);

//request  multipart/formData
export const putRequestFormData = (url, data, options = {}) => request(multPart, 'PUT', url, data, options);
export const postRequestFormData = (url, data, options = {}) => request(multPart, 'POST', url, data, options);

//request unAuth
export const postUnauthRequest = (url, data) => unAuthRequest(json, 'POST', url, data);
export const getUnauthRequest = (url, data) => unAuthRequest(json, 'GET', url);